
Solace API
==========

Pre-requisites
==============

 * apachesolr module 6.x-2.x branch must be used. It must be enabled and
   correctly configured in order for Solace API to work.

 * jQuery UI module is optional, but it helps a lot to get a nice end-user
   UI, so enable it if you can.

 * ahah_helper module is pushed to its maximum abilities, and it exists a odd
   bug, see http://drupal.org/node/480472 for patching, this is mandatory.
   Note that it may work with 2.x branch, but we only tested it with 1.x branch.

The ahah_helper bug
===================

There is a bug in ahah_helper 6.x-1.x, which a known working patch the module
maintainer never commited to head. You have to apply this patch else the AJAX
filters form won't work.

Get it there:
http://drupal.org/node/480472

Or with direct link:
http://drupal.org/files/issues/ahah_helper-480472.patch

Theming the filter form
=======================

Each filter subform has the particular 'filter-values-<filter_name>' CSS class
in order to let site themers to theme those.

Other notes
===========

Provided images comes from the "Lullacons Pack 1", licenced under GPL. see
http://www.lullabot.com/articles/free_gpl_icons_lullacons_pack_1

Some other comes from http://www.iconzplanet.com/free-onebit-icon-pack.html
It seems they dont have any licence attached, site just says: "If you enjoyed
these icons please feel free to share them!".
