
/**
 * @file
 * jQuery UI slider handler for Solace API module
 */

(function ($) {

Drupal.behaviors.AssistantInitUI = function(context) {
  $('.assistant-slider', context).each(function(){
    if (! $(this).is('.assistant-slider-processed')) {
      new Drupal.AssistantSlider(this);
      $(this).addClass('assistant-slider-processed');
    }
  });
};

/**
 * Simple jQuery UI slider handler.
 * Remember here that 'element' variable is the context to use.
 */
Drupal.AssistantSlider = function(element) {
  // Self reference to keep object in scope in anonymous callbacks
  var self = this;

  // Hidden value field
  this.valueElement = $("input.slider-value", element);
  // Default value
  this.defaultValue = parseInt(self.valueElement.val(), 10);

  // Append jQuery UI slider
  $(element).append("<div class=\"jquery-ui-slider\"></div>");
  this.sliderElement = $("div.jquery-ui-slider", element);
  this.sliderElement.slider({
    min: 0,
    max: 100,
    step: 1,
    value: self.defaultValue,
    change: function(event, ui) {
      self.valueElement.val($(this).slider('value'));
    }
  });
  this.sliderElement.slider();

  // Override an old jQueryUI Legacy bug we spotted
  if ($.ui.slider.version == "1.6") {
    $(".ui-slider-handle", element).css({
      // Really abitrary way of computing right position
      left: Math.round(($(".ui-slider", element).width() / 100 * this.defaultValue) - ($(".ui-slider-handle").width() / 2))
    });
  }
};

})(jQuery);
