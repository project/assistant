<?php

/**
 * @file
 * Solace API administration pages.
 */

function assistant_admin_build($form_state) {
  $form = array();

  $form[ASSISTANT_DEBUG_MODE] = array(
    '#type' => 'checkbox',
    '#title' => "Debug mode",
    '#description' => t("If checked, all Solr Assistant module suite will be in extreme verbose mode; some debug output will be logged using watchdog."),
    '#default_value' => assistant_debug(),
  );
  
  $form[ASSISTANT_USE_APACHESOLR_BOOST] = array(
    '#type' => 'checkbox',
    '#title' => "Use Apache Solr Integration module boost definition",
    '#description' => t("<strong>This setting is recommended.</strong>If checked, the assistant feature will always use site administrator defined Query Fields boost values.<br/>Those values are configured at !urlbias and !urlfields pages.", array(
      '!urlbias' => l(t('Content bias settings'), 'admin/settings/apachesolr/content-bias'),
      '!urlfields' => l(t('Search fields'), 'admin/settings/apachesolr/query-fields'),
    )),
    '#default_value' => variable_get(ASSISTANT_USE_APACHESOLR_BOOST, TRUE),
  );

  return system_settings_form($form);
}

function _assistant_admin_build_query_params() {
  return array(
    'q' => "Query",
    'fq' => "Filter Query",
    'q.alt' => "q.alt",
    'qf' => "Query Fields",
    'pf' => "Phrase Fields",
    'ps' => "Phrase Slop",
    'qs' => "Query Phrase Slop",
    'bq' => "Boost Query",
    'bf' => "Boost Functions",
  );
}

function assistant_admin_build_query($form_state) {
  $form = array();

  foreach (_assistant_admin_build_query_params() as $param => $name) {
    $form[$param] = array(
      '#type' => 'textfield',
      '#title' => (($name == $param) ? $param : ($param . ' (' . $name . ')')),
      '#description' => t("Use commas to separate lines from each others for multiple lines parameters."),
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Query'),
  );

  if (isset($form_state['results'])) {
    $form['results'] = array(
      '#type' => 'markup',
      '#prefix' => '<div>',
      '#suffix' => '</div>',
      '#value' => theme('assistant_preview', $form_state['results']),
    );
  }

  return $form;
}

function assistant_admin_build_query_submit($form, &$form_state) {
  $params = array();
  foreach (_assistant_admin_build_query_params() as $param => $name) {
    $value = $form_state['values'][$param];
    if (!empty($value)) {
      $parts = preg_split('/[,]+/', $value);
      if (count($parts) > 1) {
        $params[$param] = $parts;
      }
      else {
        $params[$param] = $value;
      }
    }
  }
  $form_state['results'] = assistant_api_solr_params_query($params);
  $form_state['rebuild'] = TRUE;
}
