<?php

/**
 * @file
 * Solace API hooks descriptions.
 */

/**
 * Every module that implements a custom context should respond to this hook
 * by loading their classes includes.
 */
function hook_assistant_api_context_boostrap() {
  // Sample code with default implementation.
  module_load_include('inc', 'my_module', 'MyModuleContext');
}

/**
 * Creates a specific context instance for the module.
 * 
 * Implement this hook only if you override the Assistant_ContextAbstract class
 * in order to implement specific behaviors or data to your context.
 * 
 * @param int $fid = NULL
 *   If given, this will allow the form to load the given filters configuration
 *   and populate the form with it.
 * @param boolean $save = FALSE
 *   If set to TRUE, this will alter the form behavior and add the filters save
 *   submit function, see assistant_api_filters_form() documentation
 * 
 * @return Assistant_ContextAbstract
 *   Valid Assistant_ContextAbstract instance.
 */
function hook_assistant_api_context($fid = NULL, $save = FALSE) {
  // Sample code with default implementation.
  return MyModuleContext($fid, $save);
}

/**
 * Let a chance for a context owner to alter its own query just before the
 * Solr request.
 * 
 * This method will be run only if $context['module'] contains a module name.
 * This is not a global hook.

 * If filters are fetched from database (using the 'fid' identifier) and if
 * a cached version is found, this alteration won't be run. Be carefull about
 * this and wipe out the filters cache if you need some new alterations to
 * be done.
 * 
 * @see assistant_api_filters_cache_save()
 * 
 * @param SolrQuery $query
 *   The SolrQuery instance object, just built using filters
 * @param Assistant_ContextAbstract $context
 *   Assistant_ContextAbstract instance.
 * 
 * @return void
 *   No return value for this hook.
 */
function hook_assistant_api_query_alter(SolrQuery $query, Assistant_ContextAbstract $context) {
  // Do some stuff with $query object here
}

/**
 * This hook allows you module to advertise that it provides a list of filters.
 * 
 * @see assistant_api.module
 *   Sample implementation of this hook.
 * @see AssistantFilter.inc
 *   Sample implementation of these callbacks.
 * 
 * @return array
 *   Descriptive array of filters, see full example in foo implementation below.
 *   Mandatory values are:
 *     - 'class' : Class name of filter. This one must extends
 *       Assistant_FilterAbstract.
 *   Optional values are
 *     - 'file' : PHP file name filter class is to be implemented. Note
 *       that it can be either a full path built with drupal_get_path(), or a
 *       single file name.
 *       In case of single file name, module will try load file relative to
 *       module path.
 *       If not 'file' given at all, module will just assume that callbacks are
 *       defined in the main module file.
 */
function hook_assistant_api_filters() {
  $filters = array();
  $filters['keyword_search'] = array(
    'class' => 'GlobalTextMatchAssistantFilter',
    'file' => 'assistant_api.filters.inc',
  );
  return $filters;
}
