<?php

/**
 * @file
 * Solace API UI filter form.
 */

/**
 * Real edit filter form implementation.
 * @see assistant_api_filters_form()
 */
function assistant_api_filters_form($form_state, Assistant_ContextAbstract $context = NULL, $quick_search = NULL) {
  $form = array('#tree' => TRUE);

  // Ensure loading all needed classes.
  _assistant_api_boostrap();
  drupal_add_css(drupal_get_path('module', 'assistant_api') . '/assistant_api.form.css');
  drupal_add_css(drupal_get_path('module', 'assistant_api') . '/assistant_api.ui.css');
  drupal_add_js(drupal_get_path('module', 'assistant_api') . '/assistant_api.form.js');
  // Force autocomplete behavior to be loaded, even if we don't use it, because
  // some AHAH loaded filter lines will need it later.
  drupal_add_js('misc/autocomplete.js');
  // Load required jquery javascript if jquery_ui is enabled.
  if (assistant_api_use_jquery_ui()) {
    drupal_add_js(array('AssistantForm' => array('UiEnabled' => TRUE)), 'setting');
  }
  // Register form for ahah processing through ahah_helper module
  ahah_helper_register($form, $form_state);
  // Workaround for our weird ahah helper usage
  // $form_state['storage']['#ahah_helper']['file'] = drupal_get_path('module', 'assistant_api') . '/assistant_api.form.inc';

  // Ensure we have a context.
  if (!$context) {
    $context = assistant_api_context_create();
  }

  // Initialize some variables for later use.
  $do_preview = FALSE;
  $do_keywords = $context->allowGlobalSearchBar();
  $do_new = ! $do_keywords || isset($form_state['storage']['display_new']);

  // Store the context for validation and submit functions
  // TODO: this is ugly, should find a better way to do this
  $form['context'] = array(
    '#type' => 'value',
    '#value' => $context,
  );

  // Simulate a form resubmission
  if ($quick_search) {
    $form_state['submitted'] = TRUE;
    $form_state['storage']['data'] = array();
    $form_state['storage']['keywords']['keywords_search'] = $quick_search;
  }

  // Display token module help if module is enabled.
  $allowed_token_types = $context->getAllowedTokenTypes();
  if (module_exists('token') && !empty($allowed_token_types)) {
    $form['token_help'] = array(
      '#type' => 'fieldset',
      '#title' => t('Tokens help'),
      '#collapsible' => FALSE,
    );
    $form['token_help']['general'] = array(
      '#type' => 'markup',
      '#prefix' => '<div class="help">',
      '#suffix' => '</div>',
      '#value' => t("In order to do more advanced queries, you can use tokens in any textfield provided by filters. See available tokens below."),
    );
    $form['token_help']['custom'] = array(
      '#type' => 'markup',
      '#prefix' => '<div class="help">',
      '#suffix' => '</div>',
      '#value' => $context->getTokenHelp(),
    );
    $form['token_help']['advanced'] = array(
      '#type' => 'fieldset',
      '#title' => t('See all available tokens'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['token_help']['advanced']['help'] = array(
      '#type' => 'markup',
      '#prefix' => '<div class="help">',
      '#suffix' => '</div>',
      '#value' => theme('token_help', $allowed_token_types),
    );
  }

  // Display 'Quick search' toolbar if enabled.
  if ($do_keywords) {
    $form['keywords'] = array(
      '#prefix' => '<div class="container-inline">',
      '#suffix' => '</div>',
      '#type' => 'markup',
      '#value' => '<span> </span>',
    );
    $form['keywords']['keywords_search'] = array(
      '#title' => t("Quick search"),
      '#type' => 'textfield',
      // Forcing '#name' will allow us to get it back easily with JS.
      '#id' => 'keyword_search_input', 
      '#attributes' => array('title' => t('Type in terms or phrases as you would do with any other search engine.')),
    );
    $form['keywords']['keywords_cut'] = array(
      '#type' => 'submit',
      // FIXME: Commented right now, needs better image and theming.
      //'#type' => 'image_button',
      //'#src' => drupal_get_path('module', 'assistant_api') . '/images/arrow-double-down-green.png',
      '#value' => t('Cut..'),
      '#attributes' => array('title' => t('Parse and dispatch the quick search terms into filters.')),
      '#theme' => 'assistant_button_write',
      '#ahah' => array(
        'event' => 'assistant_click', // Creates a new event type, that our JS will trigger manually
        'path' => ahah_helper_path(array('data')),
        'wrapper' => 'filters-data-wrapper',
        'effect' => 'fade',
        'method' => 'replace',
        // FIXME: find a way to disable progress 'progress' => array('type' => 'bar'),
      ),
      '#name' => 'keywords_cut',
      '#submit' => array(), // Important, this skips normal submit
    );
  }

  // This is the AHAH container for the filters.
  $form['data'] = array(
    '#type' => 'markup',
    '#prefix' => '<div id="filters-data-wrapper">',
    '#suffix' => '</div>',
    '#value' => '<span> </span>', // Foo text, so the div appears anyway
    '#theme' => 'assistant_api_filters_form_data',
  );

  /*
   * React on AHAH clicked button.
   */

  // ahah_helper processing, add or remove filter
  if ($form_state['submitted']) {
    $add = FALSE;
    $remove = FALSE;
    $storage = &$form_state['storage']['data'];

    // Check for clicked button
    if (preg_match('/^remove-([0-9]+)$/', $form_state['clicked_button']['#name'], $matches)) {
      unset($storage[(int) $matches[1]]);
      $remove = TRUE;
    }
    else if ($form_state['clicked_button']['#name'] == 'add_filter') {
      $add = TRUE;
    }
    else if ($form_state['clicked_button']['#name'] == 'live_preview') {
      $do_preview = TRUE;
    }
    else if ($form_state['clicked_button']['#name'] == 'display_new') {
      $do_new = TRUE;
    }

    // Always submit typed keywords upper, if actions was not add or remove.
    if (!$remove && !$add) {
      // It seems that, somewhere, AHAH Helper won't let us get back values that
      // does not comes from the selected AHAH container, so we need to get it
      // back manually from the POST request.
      assistant_api_quick_search_process($form_state['storage']);
    }

    $count = 0;

    // Avoid some PHP warnings
    if (empty($storage)) {
      $storage = array();
    }
    else {
      // Then we can rebuild the whole AHAH part
      foreach ($storage as $delta => &$data) {
        $name = array_shift(array_keys($data));
        // $delta is important here, it avoids a decal with remove button
        $form['data'][$delta][$name] = _assistant_api_filters_form($form_state, $context, $name, $delta, $storage[$delta][$name]);
        $count++;
      }
    }

    // Set new filter type in custom storage, if we not removing one
    if ($add && ($name = $form_state['storage']['new']['filter_name'])) {
      $values = array();
      $form['data'][$count][$name] = _assistant_api_filters_form($form_state, $context, $name, $count, $values);
    }
  }
  // Override using our context values, if any
  else if ($values = $context->getCurrentValues()) {
    $form['data'] += _assistant_api_filters_subform($form_state, $context, $values);
  }
  // Load filters if no AJAX submission processed
  else if ($fid = $context->getFid()) {
    $filters = assistant_api_filters_load($fid);
    $form['data'] += _assistant_api_filters_subform($form_state, $context, $filters);
  }

  /*
   * Handle the 'Remove' button for all displayed filter line.
   */

  foreach ($form['data'] as $delta => &$subform) {
    if (is_array($subform)) {
      // Remove button
      $button_delta = 'remove-' . $delta;
      $subform['remove'] = array(
        '#type' => 'button',
        '#value' => t("Remove"),
        '#title' => t('Remove this filter entry from filter list.'),
        '#theme' => 'assistant_button_remove',
        '#ahah' => array(
          'event' => 'click',
          'path' => ahah_helper_path(array('data')),
          'wrapper' => 'filters-data-wrapper',
          'effect' => 'none',
          'method' => 'replace',
        ),
        '#name' => $button_delta,
        '#submit' => array(), // Important, this skips normal submit
      );
    }
  }

  /*
   * Handle 'Add' button.
   */

  if ($do_new) {
    $form['new'] = _assistant_api_filters_add_subform($context);
    // This determines the AHAH container for the "Add" widget.
    $form['new']['#prefix'] = '<div class="container-inline" id="filters-new-wrapper">';
    $form['new']['#suffix'] = '</div>';
    $form['new']['add'] = array(
      '#type' => 'button',
      '#value' => t("Add"),
      '#title' => t('Add selected filter new line to your filter list.'),
      '#theme' => 'assistant_button_add',
      '#ahah' => array(
        'event' => 'click',
        'path' => ahah_helper_path(array('data')),
        'wrapper' => 'filters-data-wrapper',
        'effect' => 'none',
        'method' => 'replace',
      ),
      '#name' => 'add_filter',
      '#submit' => array(), // Important, this skips normal submit
    );
  }
  else {
    // Ensure the AHAH container displays
    $form['new'] = array(
      '#prefix' => '<div class="container-inline" id="filters-new-wrapper">',
      '#suffix' => '</div>',
      '#value' => '<span> </span>', // Foo text, so the div appears anyway
    );
    $form['keywords']['display_new'] = array(
      '#type' => 'submit',
      '#value' => t('More..'),
      '#title' => t('Display advanced filters.'),
      '#theme' => 'assistant_button_add',
      '#ahah' => array(
        'event' => 'click',
        'path' => ahah_helper_path(array('new')),
        'wrapper' => 'filters-new-wrapper',
        'effect' => 'fade',
        'method' => 'replace',
      ),
      '#name' => 'display_new',
      '#submit' => array(), // Important, this skips normal submit
    );
  }

  /*
   * Handle 'Save' button
   */

  if (($fid = $context->getFid()) && $context->allowSave()) {
    $form['fid'] = array(
      '#type' => 'value',
      '#value' => $fid,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#attributes' => array(
        'class' => 'default-submit-action',
        'title' => t('Save this filter on database and go back.'),
       ),
      '#value' => t('Save'),
      '#submit' => array('assistant_api_filters_form_save_submit'),
    );

    if (drupal_get_destination()) {
      $form['cancel'] = array(
        '#type' => 'submit',
        '#value' => t('Cancel'),
        '#attributes' => array('title' => t('Go back and leave without saving your modifications.')),
        '#submit' => array(), // Skip all submit for cancel
      );
    }
  }

  /*
   * Handle 'Preview' button.
   */

  // Set context for live preview block
  if ($context->allowPreview()) {

    $form['preview'] = array(
      '#type' => 'button',
      '#value' => t("Update preview"),
      '#attributes' => array('title' => t('Display a live preview of your query without loosing your filters.')),
      '#ahah' => array(
        'event' => 'click',
        'path' => ahah_helper_path(array('live', 'preview')),
        'wrapper' => 'assistant-api-filter-form-live-preview',
        'effect' => 'fade',
        'method' => 'replace',
      ),
      '#name' => 'live_preview',
      '#submit' => array(), // Important, this skips normal submit
    );

    if (assistant_api_use_jquery_ui()) {
      $form['preview']['#value'] = t('Show preview');
    }

    $form['live'] = array(
      '#type' => 'fieldset',
      '#title' => t('Live preview ..'),
      '#collapsible' => FALSE,
      '#attributes' => array('class' => 'assistant-dialog-main'),
    );
    $form['live']['preview'] = array(
      '#prefix' => '<div id="assistant-api-filter-form-live-preview">',
      '#suffix' => '</div>',
      '#value' => '<span> </span>', // Display this element even if empty 
    );

    if ($do_preview) {
      $form['live']['preview']['#value'] = _assistant_api_filters_form_preview($form_state['storage']['data'], $context);
    }
  }

  return $form;
}

/**
 * Force validation of all given filters.
 * 
 * This method is strictely linked to filters form shape, it exists only
 * because it's being used twice in the code.
 * 
 * @see assistant_api_filters_form_validate()
 * @see _assistant_api_filters_form_preview()
 */
function _assistant_api_filters_validate(&$filters, Assistant_ContextAbstract $context) {
  $errors = array();

  _assistant_api_boostrap();

  foreach ($filters as $delta => &$element) {

    // Remove all 'remove' buttons from values
    if (isset($element['remove'])) {
      unset($element['remove']);
    }

    // Check for validation callback
    foreach ($element as $name => &$values) {
      $filter = Assistant_FilterFactory::getFilterInstanceByName($name);
      if ($error = $filter->validate($context, $values)) {
        $element_name = 'data][' . $delta . '][' . $name . '][' . $error[0];
        $errors[$element_name] =  $error[1];
      }
    }
  }

  return $errors;
}

function assistant_api_filters_form_validate($form, &$form_state) {
  $context = $form_state['values']['context'];
  assistant_api_quick_search_process($form_state['values']);
  if (!empty($form_state['values']['data'])) {
    $errors = _assistant_api_filters_validate($form_state['values']['data'], $context);
    if (!empty($errors)) {
      foreach ($errors as $element_name => &$error) {
        form_set_error($element_name, $error);
      }
    }
  }
}

/**
 * Submit callback for filters form when save is enabled.
 */
function assistant_api_filters_form_save_submit($form, &$form_state) {
  // Save new filter
  $filters = $form_state['values']['data'];
  if (isset($form_state['values']['fid'])) {
    $filters['fid'] = $form_state['values']['fid'];
  }
  assistant_api_filters_save($filters);

  drupal_set_message(t("Filters saved"));

  // Ensure ahah_helper stuff does not return some strange array
  $form_state['rebuild'] = FALSE;
  $form_state['redirect'] = isset($_GET['destination']) ? $_GET['destination'] : $_GET['q'];

  drupal_goto($form_state['redirect']);
}

/**
 * Helper for filter form.
 */
function _assistant_api_filters_add_subform(Assistant_ContextAbstract $context) {
  $form = array();
  $options = array();
  foreach (Assistant_FilterFactory::getFilterNamesByContext($context) as $name) {
    $filter = Assistant_FilterFactory::getFilterInstanceByName($name);
    if ($filter->isEnabled($context)) {
      $options[$name] = t($filter->getTitle());
    }
  }
  $form['filter_name'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => NULL, 
  );
  return $form;
}

/**
 * Helper that build live preview result
 */
function _assistant_api_filters_form_preview(&$filters, Assistant_ContextAbstract $context) {
  $output = '';

  // Force our elements validation, do nothing if validation fails
  $errors = _assistant_api_filters_validate($filters, $context);

  if (!empty($errors)) {
    return t("Got validation errors");
  }

  // Set the context in preview mode
  $context->setPreview(TRUE);
  $service = assistant_api_solr_get_service();
  if ($service) {
    $response = $service->searchUsingFilters($filters, $context);
  }

  return theme('assistant_preview', $response, $context);
}

/**
 * Get assistant filter subform.
 */
function _assistant_api_filters_form(&$form_state, Assistant_ContextAbstract $context, $name, $delta, &$values) {
  $filter = Assistant_FilterFactory::getFilterInstanceByName($name);
  $filter->setAhahHelperPath(array('data', $delta, $name));
  if (isset($form_state['clicked_button'])) {
    if ($form_state['clicked_button']['#ahah']['path'] == $filter->getAhahHelperPath()) {
      $filter->setClickedButtonName($form_state['clicked_button']['#name']);
    }
  }
  return $filter->form($context, $values);
}

/**
 * Build filter form with user set filters for given field_name and node.
 * This will load user set filter data and build the form with those values. If
 * no values given, an empty form array is returned.
 * 
 * @param array &$form_state
 *   Real $form_state array from the global form.
 * @param Assistant_ContextAbstract $context
 *   Assistant_ContextAbstract instance.
 * @param object &$settings
 *   Assistant feature settings object for node and field_name
 * 
 * @return array
 *   Valid Form API elements subset
 */
function _assistant_api_filters_subform(&$form_state, Assistant_ContextAbstract $context, &$filters) {
  $form = array();
  if (! empty($filters)) {
    foreach ($filters as $delta => &$data) {
      if (is_array($data) && !empty($data)) {
        list($name, $values) = each($data);
        $form[] = array($name => _assistant_api_filters_form($form_state, $context, $name, count($form), $values));
      }
    }
  }
  return $form;
}

/**
 * Theme a multiple filter subform.
 * 
 * @return string
 *   (x)html output
 */
function theme_assistant_api_filter_multiple($form) {
  $output = '<div><div class="assistant-filter-multiple-add-column>"';
  $output .= drupal_render($form['add']);
  $output .= '</div><div class="assistant-filter-multiple-values-column>"';
  foreach (element_children($form['multiple']) as $key) {
    $output .= drupal_render($form['multiple'][$key]);
  }
  $output .= '</div><div class="clear-block"></div></div>';
  return $output . drupal_render($form);
}

/**
 * Theme filters building form.
 * 
 * TODO: temporary deactivated fuzzy search because it seems that SolR 1.4 has
 * some problems using it in DisMax.
 * 
 * @see theme_assistant_api_ahah_filter()
 */
function theme_assistant_api_filters_form_data($form) {
  // $headers = array(t("Filter"), t("Settings"), /* t('Fuzzy'), */ t("Boost"), "", "");
  $headers = NULL;
  $rows = array();

  // Force display even if empty
  $output = '<span> </span>';

  foreach (element_children($form) as $key) {
    // Render 'remove' button
    $row_remove = drupal_render($form[$key]['remove']);
    // Ugly weight element lookup and render
    $row_fuzzy = NULL;
    $row_boost = NULL;
    $row_title = NULL;
    $row_help = NULL;
    $row_is_filter = FALSE;
    foreach ($form[$key] as &$filter_element) {
      if (isset($filter_element['title'])) {
        $row_title = drupal_render($filter_element['title']);
      }
      if (isset($filter_element['boost'])) {
        unset($filter_element['boost']['#title']);
        $row_boost = drupal_render($filter_element['boost']);
      }
      if ($filter_element['filter_query']) {
        $row_is_filter = (bool) $filter_element['filter_query']['#value'];
      }
      if (isset($filter_element['description'])) {
        $row_help = drupal_render($filter_element['description']);
      }
      /* if (isset($filter_element['fuzzy'])) {
        unset($filter_element['fuzzy']['#title']);
        $row_fuzzy = drupal_render($filter_element['fuzzy']);
      } */
    }
    if ($row_help) {
      $row_help = '<div class="help-container"><a title="' . t("Get help") . '">' . theme('assistant_api_help_icon') . '</a>' . $row_help . '</div>';
    }
    else {
      $row_help = '';
    }
    /* if (empty($row_fuzzy)) {
      $row_fuzzy = '';
    } */
    if (empty($row_boost)) {
      $row_boost = '';
    }
    // Render all other elements
    $row_filter = $prefix . drupal_render($form[$key]) . $suffix;
    $row = array(
      'data' => array($row_title, $row_filter, /* $row_fuzzy, */ $row_boost, $row_help, $row_remove),
      'class' => ($row_is_filter ? 'filter-query' : 'query'),
    );
    $rows[] = $row;
  }

  if (!empty($rows)) {
    $output .= theme('table', $headers, $rows);
  }

  return $output . drupal_render($form);
}
