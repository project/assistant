
/**
 * @file
 * jQuery UI additions for Solace API filter form
 */

(function ($) {

Drupal.behaviors.AssistantForm = function(context) {
  $('#assistant-api-filters-form:not(.assistant-processed)', context).each(function() {
    // Ensure 'Quick search' toolbar to be emptied when the user clicks on it.
    var form = this;

    // Avoid useless AHAH loading if nothing is written.
    $('input[@name=keywords_cut]', this).click(function(event) {
      var input = $('input#keyword_search_input', form);
      if (input.val().trim().length > 0) {
        // We defined a custom event in order for us to be able to stop AHAH
        // event from being fired.
        $(this).trigger('assistant_click');
        input.val("");
      }
      event.stopPropagation();
      return false;
    });

    // Hide the more button when we display the filter select bar.
    $('input[@name=display_new]', this).click(function(event) {
      $(this).hide();
    });

    $(this).addClass('assistant-processed');
  });

  // Uses jQuery UI if present.
  if (Drupal.settings.AssistantForm != undefined && Drupal.settings.AssistantForm.UiEnabled) {
    // Attach preview dialog.
    $('#assistant-api-filters-form input[name=live_preview]:not(.assistant-processed)', context).each(function() {
      new Drupal.AssistantFormPreview(this, context);
      $(this).addClass('assistant-processed');
    });
    // Attach help dialogs.
    $('.help-container:not(.assistant-processed)', context).each(function() {
      new Drupal.AssistantFormHelpDialog(this);
      $(this).addClass('assistant-processed');
    });
  }
};

Drupal.AssistantFormHelpDialog = function(element) {
  var self = this;
  // Minor fix.
  $(element).css({
    'float': 'left',
    'margin-top': '3px',
  });
  this._dialog = false;
  var _description = $('.filter-description', element);
  _description.css({'display': 'none'});
  $('a:first', element).click(function() {
    if (!self._dialog) {
      self._dialog = _description;
      self._dialog.css({'display': 'block'});
      self._dialog.dialog({
        'width': 600,
        'height': 300,
        'title': Drupal.t('Help'),
        'modal': true
      });
    }
    else {
      self._dialog.dialog('open');
    }
  });
};

Drupal.AssistantFormPreview = function(element, context) {
  var self = this;
  this._dialog = false;
  $(element).click(function() {
    if (!self._dialog) {
      self._dialog = $('.assistant-dialog-main', context);
      self._dialog.dialog({
        'width': 800,
        'height': 400,
        'title': self._dialog.children('legend').html(),
        'modal': true
      });
    }
    else {
      $('#assistant-api-filter-form-live-preview', self._dialog).html('<p>' + Drupal.t("Loading..") + '</p>');
      self._dialog.dialog('open');
    }
  });
};

})(jQuery);
