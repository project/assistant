<?php

/**
 * @file
 * Default implementation for simple modules that don't intend to override
 * contextes.
 */

class Assistant_Context_Default extends Assistant_ContextAbstract
{
  /**
   * Module name.
   * 
   * @var string
   */
  private $__module = 'global';

  /**
   * (non-PHPdoc)
   * @see Assistant_ContextAbstract#getModule()
   */
  public function getModule() {
    return $this->__module;
  }

  /**
   * Allow usage of this implementation for any module that does not wishes to
   * override the Assistant_ContextAbstract class.
   * 
   * @param string $module
   *   Internal module name.
   */
  public function setModule($module) {
    $this->__module = $module;
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_ContextAbstract#allowPreview()
   */
  public function allowPreview() {
    return FALSE;
  }
}