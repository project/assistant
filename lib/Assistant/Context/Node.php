<?php

/**
 * @file
 * Context implementation that ensures that the given context is build arround
 * a particular node.
 */

class Assistant_Context_Node extends Assistant_Context_NodeType
{
  /**
   * Current node nid.
   * 
   * @var integer
   */
  private $__nid = NULL;

  /**
   * Set node nid for this context.
   * 
   * @param integer $nid
   *   Node nid
   */
  public function setNid($nid) {
    $this->__nid = $nid;
  }

  /**
   * Get current node nid.
   * 
   * @return integer
   *   Current node nid
   * 
   * @throws Assistant_Context_Exception
   *   If no node nid set
   */
  public function getNid() {
    if (! $this->__nid) {
      throw new Assistant_Context_Exception("No nid set");
    }
    return $this->__nid;
  }

  /**
   * Get current node
   * 
   * @return object
   *   Fully loaded node object
   * 
   * @throws Assistant_Context_Exception
   *   If no node nid set
   */
  public function getNode() {
    return node_load($this->getNid());
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_Context_NodeType#getNodeType()
   */
  public function getNodeType() {
    try {
      return parent::getNodeType();
    }
    catch (Assistant_Context_Exception $e) {
      $node = $this->getNode();
      parent::setNodeType($node->type);
      return $node->type;
    }
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_ContextAbstract#getAllowedTokenTypes()
   */
  public function getAllowedTokenTypes() {
    return array('global', 'node');
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_ContextAbstract#_getTokenObjectForType($type)
   */
  protected function _getTokenObjectForType($type) {
    switch ($type) {
      case 'node':
        return node_load($this->getNid());
      case 'global':
      default:
        return NULL;
    }
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_ContextAbstract#getTokenHelp()
   */
  public function getTokenHelp() {
    return t("Tokens relative to 'node' are information about the current assisted node.");
  }
}
