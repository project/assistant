<?php

/**
 * @file
 * Context implementation that ensures that the given context is build arround
 * a particular node type.
 */

class Assistant_Context_NodeType extends Assistant_ContextAbstract
{
  /**
   * (non-PHPdoc)
   * @see Assistant_ContextAbstract#getModule()
   */
  public function getModule() {
    return 'node';
  }
  
  /**
   * Current node type.
   * 
   * @var string
   */
  private $__nodeType = NULL;

  /**
   * Set node nid for this context.
   * 
   * @param string $node_type
   *   Node type
   */
  public function setNodeType($nodeType) {
    $this->__nodeType = $nodeType;
  }

  /**
   * Get current node nid.
   * 
   * @return integer
   *   Current node nid
   * 
   * @throws Assistant_Context_Exception
   *   If no node nid set
   */
  public function getNodeType() {
    if (! $this->__nodeType) {
      throw new Assistant_Context_Exception("No node type set");
    }
    return $this->__nodeType;
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_ContextAbstract#allowPreview()
   */
  public function allowPreview() {
    return FALSE;
  }
}
