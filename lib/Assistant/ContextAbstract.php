<?php

/**
 * @file
 * Solace API Context OOP abstraction.
 */

class Assistant_Context_Exception extends Exception { }

/**
 * Default interface for all contextes.
 * 
 * Note for developers, in order to create your own Context implementation,
 * inherit from this class and override any methods, sticking to the API
 * documentation.
 * 
 * @see assistant_api_context_create()
 */
abstract class Assistant_ContextAbstract
{
  /**
   * Additional fields you want Apache SolR instance to fetch.
   * 
   * @var array
   *   Keys are field names, to avoid duplicates. Values are foo values.
   */
  protected $_documentFields = array();

  /**
   * Add field to fetch.
   * 
   * @param string $field
   *   SolR schema field canonical name
   */
  public function addDocumentField($field) {
    $this->_documentFields[$field] = TRUE;
  }

  /**
   * Add fields to fetch.
   * 
   * @param array $fields
   *   SolR schema field canonical names array.
   */
  public function addDocumentFields($fields) {
    foreach ($fields as $field) {
      $this->_documentFields[$field] = TRUE;
    }
  }

  /**
   * Get fields to fetch.
   * 
   * @return array
   *   Array of SolR schema field canonical names.
   */
  public function getDocumentFields() {
    return array_keys($this->_documentFields);
  }

  /**
   * Store current values, in order for us to give back the same form without
   * saving filters to database.
   * 
   * @var array
   */
  protected $_values;

  /**
   * Set current values.
   * 
   * @param array $values
   *   Current values
   */
  public function setCurrentValues(&$values) {
    $this->_values = $values;
  }

  /**
   * Get current values.
   * 
   * @return array
   *   Values, or NULL if empty
   */
  public function getCurrentValues() {
    return $this->_values;
  }

  /**
   * Filters instance id.
   * 
   * @var integer
   */
  protected $_fid = NULL;

  /**
   * Return the current filters instance id.
   * 
   * @return int
   *   Filters instance id
   */
  public function getFid() {
    return $this->_fid;
  }

  /**
   * Does this context allow save button in filter form.
   * 
   * @var boolean
   */
  protected $_save = FALSE;

  /**
   * Get owner module name.
   * 
   * @return string
   *   Internal module name that owns this context
   */
  public abstract function getModule();

  /**
   * Does this context allow previews in filter form.
   * 
   * @return boolean
   *   TRUE if the context allow previews.
   */
  public abstract function allowPreview();

  /**
   * Current context execution flow is in preview mode.
   * 
   * @var boolean
   */
  private $__preview = FALSE;

  /**
   * Are we currently in preview mode?
   * 
   * @return boolean
   */
  public function isPreview() {
    return $this->__preview;
  }

  /**
   * Set the context execution flow in preview mode. This can be usefull to tell
   * hook_assistant_api_query_alter() implementations that the SolR query being
   * built is meant for preview.
   * 
   * @param boolean $toggle
   *   TRUE to set in preview mode, FALSE else
   * 
   * @throws Assistant_Context_Exception
   *   If context does not supports preview mode.
   */
  public function setPreview($toggle) {
    if (!$this->allowPreview()) {
      $this->__preview = FALSE;
      throw new Assistant_Context_Exception("This context does not supports preview");
    }
    $this->__preview = (bool) $toggle;
  }

  /**
   * Keep a track of last generated query.
   * 
   * @var array
   */
  private $__lastQuery = NULL;

  /**
   * Return last done query
   * 
   * @return array
   *   An array filled with q, fq, and other query parameters
   *   NULL if this context has not been used to build a query
   */
  public function getLastPlainQuery() {
    return $this->__lastQuery;
  }

  /**
   * Return last done query
   * 
   * @param array $params
   *   An array filled with q, fq, and other query parameters
   */
  public function setLastPlainQuery(array $params) {
    $this->__lastQuery = $params;
  }

  private $__allowTokens = FALSE;

  /**
   * Return the valid token types for current context. This will be used by
   * token into some filters.
   * 
   * @return array
   *   Array of token types
   */
  public function getAllowedTokenTypes() {
    if ($this->__allowTokens) {
      // Default context should not allow any other values that those.
      return array('global');
    }
    else {
      return array();
    }
  }

  /**
   * Get object for given token type. Any custom implementation that provides
   * an getAllowedTokenTypes() override better should implement this method in
   * order to supports their own custom tokens. 
   * 
   * @return mixed
   *   Any object. NULL if no object is required for given type.
   */
  protected function _getTokenObjectForType($type) {
    // This is a sample implementation, switch here is useless but remains a
    // good code sample for custom implementations.
    switch ($type) {
      case 'global':
      default:
        return NULL;
    }
  }

  /**
   * Proceed token replacements in given text. This is the only external entry
   * point for token feature, so this is where the token_module existance will
   * be done.
   * 
   * @param string $text
   * 
   * @return string
   *   Altered $text
   */
  public final function tokenReplace($text) {
    if (module_exists('token')) {
      foreach ($this->getAllowedTokenTypes() as $type) {
        $text = token_replace($text, $type, $this->_getTokenObjectForType($type));
      }
    }
    return $text;
  }

  /**
   * Help about your token target objets. We highly recommend any overriding
   * class to override this method if they provide extra token types.
   * 
   * @return string
   *   Human readable and localized string
   */
  public function getTokenHelp() {
    return t('All the tokens are relative to global hosting site.');
  }

  /**
   * Does this context allows save button in filter form.
   * 
   * @return boolean
   *   TRUE if the context allows save button.
   */
  public function allowSave() {
    return $this->_save;
  }

  /**
   * Does this context allows the global keyword search bar. In order for it to
   * work, the Assistant_Filter_Phrase must be enabled.
   * 
   * You can override this function, but if phrase_search is not enabled on your
   * system, you'll then have to return FALSE, be carefull.
   * 
   * @return boolean
   */
  public function allowGlobalSearchBar() {
    try {
      return Assistant_FilterFactory::isFilterEnabled($this, 'phrase_search');
    }
    catch (Assistant_FilterFactory_Exception $e) {
      return FALSE;
    }
  }

  /**
   * Default constructor.
   * 
   * @param $fid
   *   Filters instance id
   * @param boolean $save = FALSE
   *   If set to TRUE, this will alter the form behavior and add the filters save
   *   submit function, see assistant_api_filters_form() documentation
   * @param boolean $tokens = FALSE
   *   If set to TRUE, allow tokens, else disable the token feature.
   */
  public function __construct($fid = NULL, $save = FALSE, $tokens = FALSE) {
    if ($fid) {
      $this->_fid = $fid;
    }
    $this->_save = (bool) $save;
    if (! $fid && $this->_save) {
      // TODO change this when filters will have a full OOP API.
      $this->_fid = assistant_api_filters_empty_instance();
    }
    $this->__allowTokens = (bool) $tokens;
  }
}
