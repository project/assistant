<?php

/**
 * @file
 * Abstraction of multiple field filter.
 */

abstract class Assistant_Filter_MultipleAbstract extends Assistant_FilterAbstract
{

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#isBoostAble()
   */
  public final function isBoostAble() {
    return FALSE;
  }

  /**
   * This method must return a ready to be filled collection statement.
   * 
   * @return Solr_Query_Collection
   */
  protected function _getQueryCollectionStatement() {
    return new Solr_Query_Collection();
  }

  /**
   * This method must return the default operator for collection.
   * 
   * @return mixed
   *   Solr_Query_Operator::OPERATOR_OR or Solr_Query_Operator::OPERATOR_AND
   */
  protected function _getQueryCollectionOperator() {
    return Solr_Query_Operator::OPERATOR_OR;
  }

  /**
   * This method must return an Apache SolR schema field name.
   * 
   * @return string
   */
  protected abstract function _getQueryField();

  /**
   * This method must return a statement to add in the collection using the
   * given value array.
   * 
   * @param Assistant_ContextAbstract $context
   *   Assistant_ContextAbstract instance.
   * @param array &$values
   *   Values from the form filter element.
   * 
   * @return Solr_Query_StatementAbstract
   */
  protected abstract function _getQueryStatementValue(Assistant_ContextAbstract $context, &$values);

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#_build($context, $values, $query)
   * 
   * This is where the magic happens, we are going to pragmatically build the
   * collection on which to filter.
   */
  protected final function _build(Assistant_ContextAbstract $context, &$values, SolrQuery $query) {
    if (!empty($values['multiple'])) {
      $collection = $this->_getQueryCollectionStatement();
      $collection->setOperator($this->_getQueryCollectionOperator());

      foreach ($values['multiple'] as &$_values) {
        $collection->add($this->_getQueryStatementValue($context, $_values['value']));
      }

      $filter = new Solr_Query_Field($this->_getQueryField(), $collection);

      $filter->setExclusion(Solr_Query_Operator::OPERATOR_REQUIRE);
      $query->fq->add($filter);
    }
  }

  /**
   * Build the multiple subform. This method signature is the exact same that
   * the Assistant_FilterAbstract#_form($context, $values) one.
   * 
   * @see Assistant_FilterAbstract#_form($context, $values)
   */
  protected abstract function _subForm(Assistant_ContextAbstract $context, &$values = array());

  /**
   * Factorisation of multiple form subform.
   */
  private function __subForm($delta, Assistant_ContextAbstract $context, &$values = array()) {
    $subform = array(
      '#prefix' => '<div class="assistant-filter-multiple-row">',
      '#suffix' => '<div class="clear-block"></div></div>',
    );
    $subform['value'] = $this->_subForm($context, $values);
    $subform['value']['#prefix'] = '<div class="assistant-filter-multiple-value">';
    $subform['value']['#suffix'] = '</div>';
    $subform['remove'] = array(
      '#prefix' => '<div class="assistant-filter-multiple-remove">',
      '#suffix' => '</div>',
      '#type' => 'button',
      '#value' => t('Remove'),
      '#name' => 'filter-multiple-remove-' . $delta,
      '#theme' => 'assistant_button_remove',
      '#attributes' => array('class' => 'assistant-filter-multiple-remove'),
      '#submit' => array(), // Disable any submission function.
    );
    $this->setAhahProperty($subform['remove'], 'click');
    return $subform;
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#_form($context, $values)
   */
  protected final function _form(Assistant_ContextAbstract $context, &$values = array()) {
    $form = array('#theme' => 'assistant_api_filter_multiple');

    $form['multiple'] = array();

    if (preg_match('/^filter-multiple-remove-([0-9]+)$/', $this->getClickedButtonName(), $matches)) {
      unset($values['multiple'][$matches[1]]);
    }

    $delta = 0;
    if (isset($values['multiple']) && !empty($values['multiple'])) {
      foreach ($values['multiple'] as &$_values) {
        $form['multiple'][] = $this->__subForm($delta, $context, $_values['value']);
        $delta++;
      }
      if ($this->getClickedButtonName() == 'filter-multiple-add') {
        $form['multiple'][] = $this->__subForm($delta, $context);
      }
    }
    // We can reach this code after going through the last element of the
    // current set values. 
    if (empty($form['multiple'])) {
      // Always add a new value if values are empty to let user do at least one
      // input.
      $form['multiple'][] = $this->__subForm(0, $context);
    }

    // This will reset values positions in order to match the exact form we
    // generated with new deltas.
    // This ensure that these values, when the get saved in form_state will
    // have the right keys.
    $values['multiple'] = array_values($values['multiple']);

    // Handle 'Add more' button.
    $form['add'] = array(
      '#prefix' => '<div class="assistant-filter-multiple-add">',
      '#suffix' => '</div>',
      '#type' => 'button',
      '#value' => t('Add more'),
      '#name' => 'filter-multiple-add',
      '#theme' => 'assistant_button_add',
      '#attributes' => array('class' => 'assistant-filter-multiple-add'),
      '#submit' => array(), // Disable any submission function.
    );
    $this->setAhahProperty($form['add'], 'click');

    return $form;
  }

  /**
   * Validate multiple subform. This method signature is the exact same that
   * the Assistant_FilterAbstract#_validate($context, $values) one.
   * 
   * @see Assistant_FilterAbstract#_form($context, $values)
   */
  protected function _subValidate(Assistant_ContextAbstract $context, &$values) { }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#_validate($context, $values)
   */
  protected final function _validate(Assistant_ContextAbstract $context, &$values) {
    $errors = array();
    if (!empty($values['multiple'])) {
      foreach ($values['multiple'] as &$_values) {
        $local = $this->_subValidate($context, $_values['value']);
        if (!empty($local)) {
          $errors[] = $local;
        }
      }
    }
    return empty($errors) ? NULL : array('multiple', implode('<br/>', $errors));
  } 

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#isFilterQuery()
   */
  public function isFilterQuery() {
    return TRUE;
  }
}
