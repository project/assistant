<?php

/**
 * @file
 * Node type Assistant filter implementation.
 */

class Assistant_Filter_NodeType extends Assistant_FilterAbstract
{
  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#getTitle()
   */
  public function getTitle() {
    return "Content type";
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#getDescription()
   */
  public function getDescription() {
    return "Content type filter";
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#isBoostAble()
   */
  public function isBoostAble() {
    return FALSE;
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#_build($context, $values, $query)
   */
  protected function _build(Assistant_ContextAbstract $context, &$values, SolrQuery $query) {
    if (! empty($values['types'])) {
      $fields = new Solr_Query_TermCollection();
      $fields->setOperator(Solr_Query_Operator::OPERATOR_OR);
      foreach ($values['types'] as $node_type => $enabled) {
        if ($enabled) {
          $fields->add($node_type);
        }
      }
      $fieldQuery = new Solr_Query_Field('type', $fields);
      $fieldQuery->setBoost($values['boost']);
      $fieldQuery->setExclusion(Solr_Query_Operator::OPERATOR_REQUIRE);
      $query->fq->add($fieldQuery);
    }
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#_form($context, $values)
   */
  protected function _form(Assistant_ContextAbstract $context, &$values = array()) {
    $form = array();
    $form['types'] = array(
      '#type' => 'checkboxes',
      '#options' => node_get_types('names'),
      '#default_value' => $values['types'] ? $values['types'] : array(),
      '#required' => TRUE,
    );
    return $form;
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#isFilterQuery()
   */
  public function isFilterQuery() {
    return TRUE;
  }
}
