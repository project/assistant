<?php

/**
 * @file
 * Exact phrase match Assistant filter implementation.
 */

class Assistant_Filter_Phrase extends Assistant_FilterAbstract
{
  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#getTitle()
   */
  public function getTitle() {
    return "Exact phrase match";
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#getDescription()
   */
  public function getDescription() {
    return "By using this filter, you will make the search keyword based. You can apply this filter as many times as you want, each line will be an exact phrase lookup. If you want to lookup for more than one keywords, indifferently of the phrase they appear, use one line for each word.";
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#isBoostAble()
   */
  public function isBoostAble() {
    return TRUE;
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#_build($context, $values, $query)
   */
  protected function _build(Assistant_ContextAbstract $context, &$values, SolrQuery $query) {
    if (! empty($values['phrase'])) {
      $term = new Solr_Query_Term($values['phrase']);
      $term->setBoost($values['boost']);
      // $term->setFuzzyness($values['fuzzy']);
      $query->q->add($term);
    }
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#_form($context, $values)
   */
  protected function _form(Assistant_ContextAbstract $context, &$values = array()) {
    $form = array();
    $form['phrase'] = array(
      '#type' => 'textfield',
      '#default_value' => $values['phrase'],
      '#required' => TRUE,
    );
    return $form;
  }
}
