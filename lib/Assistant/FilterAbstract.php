<?php

/**
 * @file
 * Solace API Filter OOP abstraction.
 */

/**
 * Generic execption for any errors thrown by the Assistant Filters API
 */
class Assistant_Filter_Exception extends Exception { }

/**
 * Default interface for all filters.
 *
 * This class must be overriden in order to properly make a filter for the
 * system. Remember that *any* methods which is not final can (and in most
 * cases should) be overriden.
 */
abstract class Assistant_FilterAbstract
{
  /**
   * Tells if current filter is boost-able.
   */
  public function isBoostAble() {
    return FALSE;
  }

  /**
   * Tells if filter is fuzzy-able
   */
  public function isFuzzyAble() {
    return FALSE;
  }

  /**
   * Tells if filter is roaming-able
   */
  public function isRoamingAble() {
    return FALSE;
  }

  /**
   * Tells if the filter will use the filter query in order to do a really
   * restrictive query.
   *
   * Set this flag is optional, but it will alter the final UI and tell the
   * user this flag is a query killer.
   */
  public function isFilterQuery() {
    return FALSE;
  }

  /**
   * This function is run when the form is populating the 'Add' select. It gives
   * you a last chance to disable the filter if context conditions are not met.
   *
   * @param Assistant_ContextAbstract $context
   *   Current context
   * 
   * @return boolean
   *   TRUE if filter can be used in this particular context, FALSE else.
   */
  public function isEnabled(Assistant_ContextAbstract $context) {
    return TRUE;
  }

  /**
   * Process values in order to do token replacements in all string ones.
   *
   * @param Assistant_ContextAbstract $context
   *   Assistant_ContextAbstract instance.
   * @param array &$values
   *   Values from the form filter element.
   */
  public final function processValues(Assistant_ContextAbstract $context, &$values) {
    if (!empty($values)) {
      foreach ($values as $key => $value) {
        if (is_string($value) && strlen($value) > 0) {
          $values[$key] = $context->tokenReplace($value);
        }
      }
    }
  }

  /**
   * This is fired when the Solr request is built.
   *
   * @param Assistant_ContextAbstract $context
   *   Assistant_ContextAbstract instance.
   * @param array &$values
   *   Values from the form filter element. This will also contain the 'boost' and
   *   'fuzzyness' parameters.
   * @param SolrQuery $query
   *   An SolrQuery instance object, ready to use
   */
  public final function build(Assistant_ContextAbstract $context, &$values, SolrQuery $query) {
    $this->processValues($context, $values);
    $this->_build($context, $values['values'], $query);
  }

  /**
   * Build single filter subform
   *
   * @param Assistant_ContextAbstract $context
   *   Assistant_ContextAbstract instance.
   * @param array &$values = array()
   *   (optional) User set values in case of update.
   *
   * @return array
   *   Valid Form API elements subset
   */
  public final function form(Assistant_ContextAbstract $context, &$values = array()) {
    // Retrieve specific form
    $form = array();
    $form['title'] = array('#markup' => 'value', '#value' => t($this->getTitle()), '#weight' => -50);
    if ($description = $this->getDescription()) {
      $form['description'] = array(
        '#prefix' => '<div class="filter-description" style="display: none;">',
        '#suffix' => '</div>',
        '#type' => 'markup',
        '#value' => t($description),
      );
    }
    $form['values'] = $this->_form($context, $values['values']);
    $form['values']['#prefix'] = '<div id="' . $this->getValuesDivId() . '" class="filter-values-' . $this->getName() . '">';
    $form['values']['#suffix'] = '</div>';
    /* TODO deactivated fuzzyness support
     * @see theme_assistant_api_filters_form_data() PHPdoc.
    // Add fuzzyness slider if fuzzable
    if ($filter['fuzzable']) {
      $form['fuzzy'] = array(
        '#type' => 'assistant_slider',
        '#title' => t('Fuzzy/Roaming factor'),
        '#default_value' => isset($values['fuzzy']) ? $values['fuzzy'] : 0,
      );
    }
     */
    // Add boost slider if boostable
    if ($this->isBoostAble()) {
      $form['boost'] = array(
        '#type' => 'assistant_slider',
        '#title' => t('Boost factor'),
        '#default_value' => isset($values['boost']) ? $values['boost'] : 0,
      );
    }
    // This will help for theming later
    $form['filter_query'] = array(
      '#type' => 'value',
      '#value' => $this->isFilterQuery(),
    );
    return $form;
  }

  /**
   * Validate form elements.
   *
   * @param Assistant_ContextAbstract $context
   *   Assistant_ContextAbstract instance.
   * @param array &$values = array()
   *   (optional) User set values in case of update
   *
   * @return array
   *   Array filled with two values, first one is the full Form API element path,
   *   relative to this subform root (it could be something like 'foo][bar][baz').
   *   Second value is the localized error message.
   *   Return NULL if no error happens.
   */
  public function validate(Assistant_ContextAbstract $context, &$values) {
    return $this->_validate($context, $values['values']);
  }

  /**
   * Returns a list of allowed context class for this filter.
   *
   * @return array
   *   Array of context canonical class names, or NULL if filter can be used
   *   within all contextes.
   */
  public function getAllowedContextClassNames() {
    return NULL;
  }

  /**
   * Get filter title.
   * 
   * Return must NOT be localized. The API will pass it to the t() function
   * itself.
   *
   * @return string
   *   Filter title.
   */
  public abstract function getTitle();

  /**
   * Get filter description. It may be a long text. This will be displayed to
   * the end user when it clicks on the help icon on the filter form, so don't
   * be shy about details.
   *
   * Return must NOT be localized. The API will pass it to the t() function
   * itself.
   * 
   * @return string
   *   Filter description or NULL if not set.
   */
  public abstract function getDescription();

  /**
   * This is more a callback than a hook.
   * It is fired when the Solr request is built.
   *
   * <code>
   *   protected function _build(Assistant_ContextAbstract $context, &$values, SolrQuery $query) {
   *     if (! empty($values['keywords'])) {
   *       $query->q->add($values['keywords'], NULL, $values['boost']);
   *     }
   *   }
   * </code>
   *
   * @param Assistant_ContextAbstract $context
   *   Assistant_ContextAbstract instance.
   * @param array &$values
   *   Values from the form filter element. This will also contain the 'boost' and
   *   'fuzzyness' parameters.
   * @param SolrQuery $query
   *   An SolrQuery instance object, ready to use
   *
   * @return void
   *   No return value, here, you have to alter the $query object.
   */
  protected abstract function _build(Assistant_ContextAbstract $context, &$values, SolrQuery $query);

  /**
   * This is more a callback than a hook.
   * It is fired when the filter form is built.
   *
   * <code>
   *   public function form(Assistant_ContextAbstract $context, &$values = array()) {
   *     $form = array();
   *     $form['keywords'] = array(
   *       '#type' => 'textfield',
   *       '#default_value' => $values['keywords'],
   *       '#required' => TRUE,
   *     );
   *     return $form;
   *   }
   * </code>
   *
   * @param Assistant_ContextAbstract $context
   *   Assistant_ContextAbstract instance.
   * @param array &$values = array()
   *   Values from the form filter element, it can be empty in case of new element
   *   spawn in the filter form.
   * 
   * @return array
   *   A Form API valid subpart of form.
   */
  protected abstract function _form(Assistant_ContextAbstract $context, &$values = array());

  /**
   * This is more a callback than a hook.
   * It is fired when the filter form is built.
   *
   * <code>
   *   public function validate(Assistant_ContextAbstract $context, &$values = array()) {
   *     // This is foo code and will always fail
   *     return array('keywords', t('Some error'));
   *   }
   * </code>
   *
   * @param Assistant_ContextAbstract $context
   *   Assistant_ContextAbstract instance.
   * @param array &$values
   *   Values from the form filter element.
   *
   * @return array
   *   Array filled with two values, first one is the full Form API element path,
   *   relative to this subform root (it could be something like 'foo][bar][baz').
   *   Second value is the localized error message.
   *   Return NULL if no error happens.
   */
  protected function _validate(Assistant_ContextAbstract $context, &$values) { }

  /**
   * Ahah helper path of current element.
   *
   * @var array
   */
  private $__ahahHelperPath = NULL;

  /**
   * Set Ahah helper path of current element.
   *
   * @param array $path
   */
  public final function setAhahHelperPath(array $path) {
    $this->__ahahHelperPath = $path;
    $this->__ahahHelperPath[] = 'values';
  }

  /**
   * Get Ahah helper path of current element.
   *
   * @return string
   */
  public final function getAhahHelperPath() {
    return ahah_helper_path($this->__ahahHelperPath);
  }

  /**
   * Get computed div id using ahah_helper path.
   *
   * @return string
   *
   * @throws Assistant_Filter_Exception
   *   If ahah helper path is not set
   */
  public final function getValuesDivId() {
    if (!$this->__ahahHelperPath) {
      throw new Assistant_Filter_Exception("No Ahah helper path set");
    }
    return str_replace('_', '-', implode('-', $this->__ahahHelperPath));
  }

  /**
   * Set ahah property of current element. This will allow subfilters to use
   * AHAH. Note that because of form complexity, you can only set the changing
   * event, and take care yourself about what to do when the _form() method is
   * called using values.
   *
   * Note that values are always stored into form storage, so what you get at
   * form build time is the current state of form, even in AJAX context (with
   * values filled).
   *
   * @param array $element
   *   The form element on which you want to apply the #ahah property.
   * @param string $event = 'change'
   *   (optional) The event on which to react.
   */
  public final function setAhahProperty(&$element, $event = 'change') {
    $element['#ahah'] = array(
      'event' => 'click',
      'path' => $this->getAhahHelperPath(),
      'wrapper' => $this->getValuesDivId(),
      'effect' => 'none',
      'method' => 'replace',
    );
  }

  /**
   * Clicked button #name property, usefull if you are developing an AHAH based
   * filter.
   * 
   * @var string
   */
  private $__clickedButtonName = NULL;

  /**
   * Set the clicked button name.
   * 
   * Notice: external filter developer should never call this method by itself.
   * The main form will put a value here only if clicked button has the same
   * AHAH path that the filter value.
   * 
   * @param string $name
   *   Clicked button name.
   */
  public final function setClickedButtonName($name) {
    $this->__clickedButtonName = $name;
  }

  /**
   * Get the clicked button name, if it was in the current filter. Always use
   * this function when you are developing a custom filter to avoid multiple
   * filter instances conflicts.
   * 
   * @return string
   *   Clicked button name.
   */
  public final function getClickedButtonName() {
    return $this->__clickedButtonName;
  }

  /**
   * Filter internal name.
   *
   * @var string
   */
  private $_name = NULL;

  /**
   * Get filter internal name.
   *
   * @return string
   *   Filter internal name.
   */
  public final function getName() {
    return $this->_name;
  }

  /**
   * Constructor.
   *
   * @param string $name
   *   Internal filter name.
   */
  public final function __construct($name) {
    $this->_name = (string) $name;
  }
}
