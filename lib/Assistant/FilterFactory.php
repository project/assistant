<?php

/**
 * @file
 * Filter factory helper.
 */

/**
 * Exception thrown by Assistant_FilterFactory.
 */
class Assistant_FilterFactory_Exception extends Assistant_Filter_Exception { }

/**
 * Factory class that allows to bypass Drupal API for some common operations.
 * Note that it must be initialized before every usage.
 *
 * @see _assistant_api_boostrap()
 */
class Assistant_FilterFactory
{
  /**
   * Static cache of internal names for rapid access.
   *
   * @var array
   */
  private static $__names = NULL;

  /**
   * Static cache of custom module given filter descriptions.
   *
   * @var array
   */
  private static $__cache = NULL;

  /**
   * Lazzy loaded instances static cache.
   *
   * @var array
   */
  private static $__instances = array();

  /**
   * Get all supported filter names.
   *
   * @return array
   *   Array of internal filter name.
   */
  public static function getFilterNamesAll() {
    return self::$__name;
  }

  /**
   * Static initialization. We'll keep this one lazzy to ensure we won't do any
   * unneeded operations.
   *
   * Drupal side will have to initialize this object using the cache.
   *
   * @param array $cache
   *   Key/value pairs. Keys are filter names, values are descriptive array
   *   resulting of cache construction, with invalid cuustom filter declarations
   *   excluded.
   */
  public static function init(&$cache) {
    self::$__cache = &$cache;
    self::$__names = array_keys(self::$__cache);
  }

  /**
   * Tells if the current filter is enabled for this context.
   * 
   * @param string $name
   *   Filter name
   * 
   * @return boolean
   */
  public static function isFilterEnabled(Assistant_ContextAbstract $context, $name) {
    $filter = self::getFilterInstanceByName($name);
    $contextes = $filter->getAllowedContextClassNames();
    if (!$contextes) {
      return TRUE;
    }
    foreach ($contextes as $className) {
      if (get_class($context) == $className || is_subclass_of($context, $className)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Get specific filter instance by name.
   *
   * @param string $name
   *   Filter name
   * @param Assistant_ContextAbstract $context = NULL
   *   (optional) Assistant_ContextAbstract instance on which the filter should
   *   react.
   *
   * @return Assistant_FilterAbstract
   *   Assistant_FilterAbstract specific implementation.
   *   NULL if filter does not exists.
   *
   * @throws Assistant_FilterFactory_Exception
   *   If filter name does not exists
   */
  public static function getFilterInstanceByName($name) {
    // Check filter name is registered
    if (!isset(self::$__cache[$name])) {
      throw new Assistant_FilterFactory_Exception("Unknown filter name: " . (string) $name);
    }

    if (! isset(self::$__instances[$name])) {
      // Proceed to lazzy instanciation.
      $desc = &self::$__cache[$name];
      if (isset($desc['file'])) {
        require_once $desc['file'];
      }
      self::$__instances[$name] = new $desc['class']($name);
    }

    return self::$__instances[$name];
  }

  /**
   * Get specific filters instances available under the given context.
   *
   * @return array
   *   Array of Assistant_FilterAbstract instances
   */
  public static function getFilterNamesByContext(Assistant_ContextAbstract $context) {
    $ret = array();

    // FIXME: This could be a lot faster and avoid so much objets instanciation.
    foreach (self::$__cache as $name => &$desc) {
      $filter = self::getFilterInstanceByName($name);
      $contextes = $filter->getAllowedContextClassNames();
      if (empty($contextes)) {
        $ret[] = $name;
      }
      else {
        foreach ($contextes as $className) {
          if (get_class($context) == $className || is_subclass_of($context, $className)) {
            $ret[] = $name;
            break;
          }
        }
      }
    }

    return $ret;
  }
}
