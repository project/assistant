<?php

/**
 * @file
 * Solace API SolR query abstraction. This is the glue between the Apache SolR
 * service and Assistant context.
 */

class Assistant_Solr_Query_Exception extends Exception { }

class Assistant_Solr_Query
{
  /**
   * Current context.
   * 
   * @var Assistant_ContextAbstract
   */
  private $__context = NULL;

  /**
   * Get current context.
   * 
   * @return Assistant_ContextAbstract
   */
  public function getContext() {
    // Lazzy instanciation if no context given
    if (!$this->__context) {
      $this->__context = assistant_api_context_create();
    }
  }

  /**
   * Get the Apache SolR service singleton.
   * 
   * @return Assistant_Solr_Service
   *   Service instance, or NULL in case of error.
   */
  protected function _getService() {
    return assistant_api_solr_get_service();
  }

  /**
   * Build the query using given filters array.
   * 
   * @param array &$filters
   *   Filters array.
   * 
   * @return Solr_QueryString
   *   SolR Query instance.
   *   
   * @throws Assistant_Solr_Query_Exception
   *   If empty filter given.
   */
  protected function _buildQuery(&$filters) {
    $query = new SolrQuery();

    if (isset($filters) && !empty($filters)) {
      // Use the lucene parser in order to ensure that boost, fuzzyness and roaming
      // will work as expected.
      $query->setParser(SolrQuery::PARSER_LUCENE);

      foreach ($filters as &$data) {
        // May remain some junk here
        if (is_array($data)) {
          // Call all filters callback build functions
          foreach ($data as $name => &$values) {
            // Query and FilterQuery strings
            try {
              Assistant_FilterFactory::getFilterInstanceByName($name)->build($context, $values, $query);
            }
            catch (Assistant_Filter_Exception $e) {
              watchdog('assistant_api', "Caught an exception during filters building: " . $e->getMessage(), NULL, WATCHDOG_ALERT);
            }
            catch (Solr_Query_Exception $e) {
              watchdog('assistant_api', "Caught an exception during filters building: " . $e->getMessage(), NULL, WATCHDOG_ALERT);
            }
          }
        }
      }
    }
    else {
      // Can do a query with an empty filter.
      throw new Assistant_Solr_Query_Exception("Empty filters given");
    }

    return $query;
  }

  /**
   * Do search.
   * 
   * @param mixed &$data
   *   Can be one of these:
   *     - Arbitrary param array (with Apache SolR options already set)
   *     - Filter array
   *     - Filter id integer
   * @param int $limit = SOLR_DEFAULT_LIMIT
   *   Number of documents to fetch per page.
   * @param int $page = 0
   *   Page number to fetch.
   * 
   * @return Assistant_Solr_Response
   *   Instance carrying the results and some other information.
   * 
   * @throws Assistant_Solr_Query_Exception
   *   In case of any error.
   */
  public function search(&$data, $limit = SOLR_DEFAULT_LIMIT, $page = 0) {
    $filters = NULL;
    $params = NULL;

    // Try to guess what $data is.

    if (is_numeric($data)) {
      $filters = assistant_api_filters_load($data);
      if (!$filters) {
        throw new Assistant_Solr_Query_Exception("Could not load filters instance, fid is: " . $data);
      }
      $params = $this->_buildQuery($filters)->getParams();
    }

    else if (is_array($data)) {
      if (TRUE) { // FIXME find a relyable method to determine what it is.
        $params = $this->_buildQuery($data)->getParams();
      }
      else {
        // Proceed to array recopy, to ensure we are not modifying what the user
        // is currently working on.
        $params = $data;
      }
    }

    else {
      throw new Assistant_Solr_Query_Exception("Wrong data given");
    }

    $context = $this->getContext();

    // Let a chance for context owner to alter its own filters.
    if (($module = $context->getModule()) != 'global') {
      $func = $module . '_assistant_api_query_alter';
      if (function_exists($func)) {
        $func($query, $context);
      }
    }

    // Apply default parameters to ensure we always have an expected result.
    $context->addDocumentField('nid');
    $context->addDocumentField('score');
    $params['fl'] = implode(',', $context->getDocumentFields());
    $params['op'] = SOLR_DEFAULT_OP;
    $params['rows'] = $limit;

    // Simple security check for page number.
    $page = $page < 0 ? 0 : (int) $page;

    $service = $this->_getService();

    // Apply apachesolr module default boost params.
    if (variable_get(ASSISTANT_USE_APACHESOLR_BOOST, TRUE)) {
      apachesolr_search_add_boost_params($params, $params['q'], $service);
    }

    $response = $service->search((isset($params['q']) ? $params['q'] : ''), $page, $limit, $params);

    return new Assistant_Solr_Response($response);
  }

  /**
   * Constructor.
   * 
   * @param Assistant_ContextAbstract $context = NULL
   *   (optional) Context to use. Leave empty to use an empty context.
   */
  public function __construct(Assistant_ContextAbstract $context = NULL) {
    if ($context) {
      $this->__context = $context;
    }
  }
}
