<?php

/**
 * @file
 * Solace API SolR results representation.
 */

class Assistant_Solr_Response
{
  /**
   * Maximum score got when querying.
   * 
   * @var float
   */
  public $maxscore = 0;

  /**
   * Total number of document matching the query.
   * 
   * @var int
   */
  public $total = 0;

  /**
   * Number of document fetched.
   * 
   * @var int
   */
  public $count = 0;

  /**
   * Array of results.
   * 
   * @var array
   *   Each value is an key/value pairs array, keys are field names and values
   *   are fetched field content.
   */
  public $results = array();

  /**
   * Ensures readonly for parameters.
   */
  public function __set($name, $value) {
    throw new Exception('Fields are readonly');
  }

  /**
   * Constructor.
   * 
   * @param Apache_Solr_Response $response
   *   Apache Solr PHP service response
   */
  public function __construct(Apache_Solr_Response $response) {
    $this->total = $response->response->numFound;

    // Skip if empty results
    if ($this->total == 0) {
      return;
    }

    $this->maxscore = $response->response->maxScore;

    foreach ($response->response->docs as $doc) {
      $data = array();
      foreach ($doc as $field => &$value) {
        $field = trim($field);
        $data[$field] = $value;
      }
      $this->results[] = $data;
    }

    $this->count = count($this->results);
  }
}
