<?php

/**
 * @file
 * Solace API SolR Service class.
 */

/*
 * FIXME: This class could be a lot better. This is only a start.
 */
class Assistant_Solr_Service extends Drupal_Apache_Solr_Service 
{
  /**
   * Request SolR using given filters configuration array formatted like node
   * ones.
   * 
   * @param int|object $filters
   *   Fid or array of filters
   *   Note that filter cache will work only if you give a numeric fid
   * @param Assistant_ContextAbstract $context = NULL
   *   (optional) Assistant_ContextAbstract instance.
   *   If none given, will create a default instance
   * @param int $limit = 10
   *   (optional) Number of results to return
   * @param int $page = 0
   *   (optional) Page number to display
   * 
   * @return Assistant_Solr_Response
   *   Object fill with results
   *   FALSE if filters are not configured, or in case of error
   */  
  public function searchUsingFilters($filters, Assistant_ContextAbstract $context = NULL, $limit = SOLR_DEFAULT_LIMIT, $page = 0) {
    $has_context = !empty($context);

    // Check for cached results
    if (is_numeric($filters) && !$filters = assistant_api_filters_load($filters)) {
      watchdog('assistant_api', "Unexistant filter", array(), WATCHDOG_ERROR);
      return FALSE;
    }

    // Context check
    if (!$has_context) {
      $context = assistant_api_context_create();
    }

    $query = new SolrQuery($params);
    // Use the lucene parser in order to ensure that boost, fuzzyness and roaming
    // will work as expected.
    $query->setParser(SolrQuery::PARSER_LUCENE);

    if (isset($filters) && !empty($filters)) {
      foreach ($filters as &$data) {
        // May remain some junk here
        if (is_array($data)) {
          // Call all filters callback build functions
          foreach ($data as $name => &$values) {
            // Query and FilterQuery strings
            try {
              Assistant_FilterFactory::getFilterInstanceByName($name)->build($context, $values, $query);
            }
            catch (Assistant_Filter_Exception $e) {
              watchdog('assistant_api', "Caught an exception during filters building: " . $e->getMessage(), NULL, WATCHDOG_ALERT);
            }
            catch (Solr_Query_Exception $e) {
              watchdog('assistant_api', "Caught an exception during filters building: " . $e->getMessage(), NULL, WATCHDOG_ALERT);
            }
          }
        }
      }
    }
    else {
      // Can do a query with an empty filter.
      return FALSE;
    }

    // Let a chance for context owner to alter its own filters
    if (($module = $context->getModule()) != 'global') {
      $func = $module . '_assistant_api_query_alter';
      if (function_exists($func)) {
        $func($query, $context);
      }
    }

    $params = $query->getParams();

    if ($has_context && !empty($params)) {
      $context->setLastPlainQuery($params);
    }

    return $this->searchUsingParams($params, $context, $limit, $page);
  }

  /**
   * Request SolR using given arbitrary given params array. 
   * 
   * @param array $params
   *   Params array ready to give to apachesolr module service for searching.
   * @param Assistant_ContextAbstract $context = NULL
   *   (optional) Assistant_ContextAbstract instance.
   *   If none given, will create a default instance
   * @param int $limit = 10
   *   (optional) Number of results to return
   * @param int $page = 0
   *   (optional) Page number to display
   * 
   * @return Assistant_Solr_Response
   *   Object fill with results
   *   FALSE if filters are not configured, or in case of error
   */
  public function searchUsingParams($params, Assistant_ContextAbstract $context = NULL, $limit = SOLR_DEFAULT_LIMIT, $page = 0) {
    // Context check
    if (!$context) {
      $context = assistant_api_context_create();
    }

    // Apply rows and fl parameters
    $context->addDocumentField('nid');
    $context->addDocumentField('score');
    $params['fl'] = implode(',', $context->getDocumentFields());
    $params['op'] = SOLR_DEFAULT_OP;
    $params['rows'] = $limit;

    // Simple security check for page number.
    $page = $page < 0 ? 0 : (int) $page;

    try {
      // Apply apachesolr module default boost params.
      if (variable_get(ASSISTANT_USE_APACHESOLR_BOOST, TRUE)) {
        apachesolr_search_add_boost_params($params, $params['q'], $this);
      }

      $response = $this->search((isset($params['q']) ? $params['q'] : ''), $page * $limit, $limit, $params);

      return new Assistant_Solr_Response($response);
    }
    catch (Exception $e) {
      // In some cases, 'apachesolr' module leaves some exceptions uncaught
      watchdog('assistant_api', "Could not contact Solr server with message '@message'", array('@message' => $e->getMessage()), WATCHDOG_CRITICAL);
    }

    return FALSE;
  }
}
