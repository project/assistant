<?php

/**
 * @file
 * Solace API Query object.
 */

/**
 * Solr params representation. This class is the link with the 'apachesolr'
 * module query object.
 */
class SolrParams
{
  /**
   * Key/value array of params to give to Solr. If a key have more than one
   * value, put an array of values as value.
   */
  protected $_params = NULL;

  /**
   * Get Solr params formated array.
   * 
   * @return array
   *   Solr params formated for 'apachesolr' module.
   */
  public function getParams() {
    return $this->_params;
  }

  /**
   * Remove a Solr param.
   * 
   * @param string $key
   *   Param name to remove
   * @param string $value
   *   (optional) Solr param value to be removed. If null given, remove all
   *   params matching $key.
   */
  public function removeParam($key, $value = NULL) {
    if (! $value) {
      unset($this->_params[$key]);
    }
    else {
      if (is_array($this->_params[$key])) {
        foreach ($this->_params[$key] as $_key => $_value) {
          if ($_value == $value) {
            unset($this->_params[$_key]);
          }
        }
      }
      else if($this->_params[$key] == $value) {
        unset($this->_param[$key]);
      }
    }
  }

  /**
   * Add a Solr param to our query
   * 
   * @param string $key
   *   Solr param name
   * @param array|string $value
   *   Solr param value, likely more a string any other type. If an array is
   *   given, this will add a set of values for the given param name.
   */
  public function addParam($key, $value) {
    if (isset($this->_params[$key])) {
      if (is_array($value)) {
        // array_values() will ensure we have numeric keys and ensure the
        // array_merge() expected behavior.
        if (is_array($this->_params[$key])) {
          $this->_params = array_merge($this->_params[$key], array_values($value));
        }
        else {
          $this->_params = array_merge(array($this->_params[$key]), array_values($value));
        }
      }
      else {
        if (is_array($this->_params[$key])) {
          $this->_params[$key][] = $value;
        }
        else {
          $this->_params[$key] = array($this->_params[$key], $value);
        }
      }
    }
    else {
      // Whatever is a single value or an array, this will do the right thing.
      $this->_params[$key] = $value;
    }
  }

  /**
   * Merge params array with existing one.
   * 
   * @param array $params = array()
   *   New params to add
   * @param boolean $override = FALSE
   *   If set to TRUE, incomming params key will override existing ones
   */
  public function addParams($params = array(), $override = FALSE) {
    foreach ($params as $key => &$value) {
      if ($override) {
        $this->removeParam($key);
      }
      $this->addParam($key, $value);
    }
  }

  /**
   * Construct the object
   * 
   * @param array $params = array()
   *   Apache Solr module formated array of params
   */
  public function __construct($params = array()) {
    $this->_params = $params;
  }
}

/**
 * Because Solr_Base_Query does not allows us to do full Solr queries, we done
 * our own simple implementation that fits to our needs.
 */
class SolrQuery
{
  const PARSER_LUCENE = 'lucene';
  const PARSER_DISMAX = 'dismax';
  const PARSER_DEFAULT = NULL;

  /**
   * Parser to use
   * 
   * @var string
   */
  protected $_parser = SolrQuery::PARSER_DEFAULT;

  /**
   * Set the parser to use for query.
   * 
   * @param string $parser
   *   One of the SolrQuery::PARSER_* constants
   */
  public function setParser($parser) {
    $this->_parser = $parser;
  }

  /**
   * Get the current query parser.
   * 
   * @return string
   *   One of the SolrQuery::PARSER_* constants or NULL if set to default.
   */
  public function getParser() {
    return $this->_parser;
  }

  /**
   * Solr params array
   * 
   * @var SolrParams
   */
  protected $_params = NULL; 

  /**
   * Solr Query
   * 
   * @var Solr_QueryString
   */
  protected $_q = NULL;

  /*
   * Solr Query Filter
   * 
   * @var Solr_QueryString
   *   Array of Solr_QueryString
   */
  protected $_fq = NULL;

  /**
   * Cached params return.
   * 
   * @var array
   */
  protected $_cached = NULL;

  /**
   * Wipe out cached built params.
   */
  protected function _wipeCache() {
    $this->_cached = NULL;
  }

  /**
   * Query getter
   */
  public function __get($name) {
    $this->_wipeCache();
    if ($name == 'q') {
      if (empty($this->_q)) {
        $this->_q = new Solr_QueryString();
      }
      return $this->_q;
    }
    else if ($name == 'fq') {
      if (empty($this->_fq)) {
        $this->_fq = new Solr_QueryString();
      }
      return $this->_fq;
    }
    else if ($name == 'params') {
      return $this->_params;
    }
    else {
      throw new Solr_Query_Exception("Unsupported parameter " . $name);
    }
  }

  /**
   * Query setter
   */
  public function __set($name, $value) {
    $this->_wipeCache();
    if ( $name == 'q') {
      if (! $value instanceof Solr_QueryString) {
        throw new Solr_Query_Exception("'q' parameter must be a Solr_QueryString instance");
      }
      $this->_q = $value;
    }
    if ( $name == 'fq') {
      if (! $value instanceof Solr_QueryString) {
        throw new Solr_Query_Exception("'fq' parameter must be a Solr_QueryString instance");
      }
      $this->_fq = $value;
    }
    else {
      throw new Solr_Query_Exception("Unsupported parameter");
    }
  }

  /**
   * Rebuild params using setted query strings
   */
  protected function _rebuildParams() {
    // Clean the query
    $this->_params->removeParam('q');
    $this->_params->removeParam('q.alt');
    $this->_params->removeParam('fq');
    // Build queries
    if ($str = (string) $this->_q) {
      // Force the parser type
      if ($this->_parser) {
        $str = '{!' . $this->_parser . '}' . $str;
      }
      $this->_params->addParam('q', $str);
      if ($str = (string) $this->_fq) {
        $this->_params->addParam('fq', $str);
      }
    }
    else {
      // Because our 'q' may be empty, use 'q.alt' to do a match all query
      if ($str = (string) $this->_fq) {
        $this->_params->addParam('q.alt', $str);
      }
      // Else silent error, let SolR returns itself that query is erroneous
    }
  }

  /**
   * Get params rebuilt using queries
   */
  public function getParams() {
    if (!$this->_cached) {
      $this->_rebuildParams();
      $this->_cached = $this->_params->getParams();
    }
    return $this->_cached; 
  }

  /**
   * Construct the object
   * 
   * @param array $params = array()
   *   Apache Solr module formated array of params
   * @param Solr_QueryString $q = NULL
   *   A Solr_QueryString instance for Solr Query string construction
   * @param Solr_QueryString $fq = NULL
   *   A Solr_QueryString instance for Solr FilterQuery string construction
   */
  public function __construct($params = array()) {
    $this->_params = new SolrParams($params);
  }
}

