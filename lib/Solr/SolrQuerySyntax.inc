<?php

/**
 * @file
 * Solace API Solr Query syntax classes.
 */

// FIXME: Need to cut this file into pieces.

/**
 * Error when building a wrong query.
 */
class Solr_Query_Exception extends Exception {}

/**
 * Static container for common static methods
 */
class Solr_Query_Operator
{
  /**
   * Require operator
   */
  const OPERATOR_REQUIRE = '+';

  /**
   * Prohibit operator
   */
  const OPERATOR_PROHIBIT = '-';

  /**
   * Boost operator
   */
  const OPERATOR_BOOST = '^';

  /**
   * Fuzzyness / roaming operator
   */
  const OPERATOR_FUZZY_ROAMING = "~";

  /**
   * And operator
   */
  const OPERATOR_AND = 'AND';

  /**
   * Or operator
   */
  const OPERATOR_OR = 'OR';

  /**
   * '*' wildcard
   */
  const WILDCARD_ALL = '*';
}

/**
 * Abstraction of a statement on which we can apply boost and boolean operators. 
 */
abstract class Solr_Query_StatementAbstract
{
  /**
   * Regex that matches all Lucene query syntax reserved chars
   */
  private static $_reSpecials = '/\+-&\|!\(\)\{\}\[\]\^"~\*\?:\\\/';

  /**
   * Add '"' chars if necessary to a token value, and escape Lucene query
   * syntax reserved chars.
   * 
   * @param string $token
   *   String to escape
   * @param boolean $force = FALSE
   *   Escape whatever happens
   * 
   * @return string
   */
  public static function escapeToken($token, $force = FALSE) {
    $_token = preg_replace(self::$_reSpecials, '\\\\\\0', $token);
    if ($force || preg_match('/ /', $_token) || strlen($token) != strlen($_token)) {
      return '"' . $_token . '"';
    }
    else {
      return $_token;
    }
  }
  
  /**
   * Convert given boost factor to some incredibly arbitrary value.
   */
  public static function boostFactor($boost) {
    /*
    $boost = (float) $boost;
    // Consider that > 1 values are given to simulate a negative boost
    // If boost is lower than 100, apply our boost huge factor method.
    if ($boost < 100 && $boost > 1) {
      // Quite ugly, but larger values gives stuff ultra negative answers for
      // all non boosted terms.
      $boost *= 10;
    }
    */
    return $boost;
  }

  /**
   * REQUIRE or PROHIBIT operator
   */
  protected $_exclusion = NULL;

  /**
   * Set exclusion mode.
   * 
   * @param string $exclusive = NULL
   *   Solr_Query_Operator::OPERATOR_REQUIRE
   *   or Solr_Query_Operator::OPERATOR_PROHIBIT
   *   or NULL
   *
   * @throws Solr_Query_Exception
   *   In case of errors
   */
  public function setExclusion($exclusion) {
    if (!empty($exclusion) && $exclusion != Solr_Query_Operator::OPERATOR_PROHIBIT && $exclusion != Solr_Query_Operator::OPERATOR_REQUIRE) {
      throw new Solr_Query_Exception("Exclusion must be Solr_Query_Operator::OPERATOR_REQUIRE or Solr_Query_Operator::OPERATOR_PROHIBIT");
    }
    $this->_exclusion = $exclusion;
  }
  
  /**
   * Boost float value, NULL for default
   */
  protected $_boost = NULL;

  /**
   * Set exclusion mode.
   * 
   * @param float $boost = NULL
   *   (optional) Float superior to 0 or NULL
   *
   * @throws Solr_Query_Exception
   *   In case of errors
   */
  public function setBoost($boost) {
    if (!empty($boost) && $boost <= 0) {
      throw new Solr_Query_Exception("Boost must be a absolute positive float, value is : " . print_r($boost, TRUE));
    }
    $this->_boost = (float) $boost;
  }

  /**
   * Return __toString() equivalent of subclass Solr syntax.
   * 
   * @return string
   */
  protected abstract function _rawOutput();

  /**
   * Escape and apply boost and operators surrounding the given string.
   *
   * @return string
   */
  public function __toString() {
    $rawOutput = trim($this->_rawOutput());
    if (! empty($rawOutput)) { 
      if (! empty($this->_exclusion)) {
        $rawOutput = $this->_exclusion . $rawOutput;
      }
      else if (!empty($this->_boost)) {
        $rawOutput .= '^' . Solr_Query_StatementAbstract::boostFactor($this->_boost);
      }
      return $rawOutput;
    }
    return '';
  }
}

/**
 * Abstraction of statement on which the fuzzyness or roaming operator
 * can be applied.
 */
abstract class Solr_Query_FuzzyStatementAbstract extends Solr_Query_StatementAbstract
{
  /**
   * Fuzzyness or roaming value. Those two parameters uses the same operator
   * are exclusive, it does not matter weither you use one or the other, use
   * case is the same the differences takes place on what kind of element on
   * which you apply.
   * 
   * TODO: see Lucene specification, fuzzyness/roaming may be set on other
   * statements than term or phrases.
   * 
   * @var int
   *   Positive integer value
   */
  protected $_fuzzyness = NULL;

  /**
   * Set fuzzyness or roaming value, both uses the same operator, only the
   * type of data (phrase or term) on which you apply it matters.
   * 
   * @param int $fuzzyness
   *   Positive integer or NULL to unset
   */
  public function setFuzzyness($fuzzyness) {
    if ($fuzzyness != NULL && (! is_numeric($fuzzyness) || $fuzzyness < 0)) {
      throw new Solr_Query_Exception("Fuzyness/roaming value must be a positive integer, " . print_r($fuzzyness, TRUE) . " given");
    }
    $this->_fuzzyness = $fuzzyness;
  }

  /**
   * Alias for setFuzzyness() method.
   * 
   * @see Solr_Query_Term#setFuzzyness()
   * 
   * @param int $roaming
   *   Positive integer
   */
  public function setRoaming($roaming) {
    $this->setFuzzyness($roaming);
  }

  /**
   * Better to use an override here else it becomes complex to handle roaming
   * and boost order applyance on statement.
   *
   * @return string
   */
  public function __toString() {
    $rawOutput = trim($this->_rawOutput());
    if (! empty($rawOutput)) { 
      if (! empty($this->_exclusion)) {
        $rawOutput = $this->_exclusion . $rawOutput;
      }
      else {
        if (!empty($this->_fuzzyness)) {
          $rawOutput .= '~' . $this->_fuzzyness;
        }
        if (!empty($this->_boost)) {
          $rawOutput .= '^' . Solr_Query_StatementAbstract::boostFactor($this->_boost);
        }
      }
      return $rawOutput;
    }
    return '';
  }
}

/**
 * Represent a collection of printable terms.
 */
class Solr_Query_Collection extends Solr_Query_StatementAbstract
{
  /**
   * Sub queries storage.
   * 
   * @var array
   */
  protected $_elements = array();

  /**
   * Adds an element to the internal list.
   * 
   * @param Solr_Query_StatementAbstract $element
   */
  public function add(Solr_Query_StatementAbstract $element) {
    $this->_elements[] = $element;
  }

  /**
   * Get textual operator.
   * 
   * @return string
   *   Textual operator, surrounded by whitespaces or only one whitespace in
   *   case operator is not set (default behavior is to use default query
   *   operator specified in Solr params).
   */
  protected function _getTextualOperator() {
    return ($this->_operator ? (' ' . $this->_operator . ' ') : ' ');
  }

  /**
   * Remove all occurences of given element if instance is found in the
   * collection.
   * 
   * @param object $element
   */
  protected function removeElement($element) {
    foreach ($this->_elements as $key => $_element) {
      if ($_element === $element) {
        unset($this->_elements[$key]);
      }
    }
  }

  /**
   * Default operator. Can be 'AND' or 'OR'.
   * 
   * @var string
   */
  protected $_operator = NULL;

  /**
   * Set default operator.
   * 
   * @param string $operator
   *   Can be 'AND' or 'OR'.
   */
  public function setOperator($operator) {
    if ($operator == NULL || $operator == Solr_Query_Operator::OPERATOR_AND || $operator == Solr_Query_Operator::OPERATOR_OR) {
      $this->_operator = $operator;
    }
    else {
      throw new Solr_Query_Exception("Wrong operator " . print_r($operator, TRUE));
    }
  }

  /**
   * Get current operator.
   * 
   * @return string $operator
   *   Can be 'AND' or 'OR', or NULL for default.
   */
  public function getOperator() {
    return $this->_operator;
  }

  /**
   * The reason why this class exists. Returns the full element list using
   * the given configured operator, or none if none where given.
   */
  protected function _rawOutput() {
    if (! empty($this->_elements)) {
      if (count($this->_elements) > 1) {
        return '(' . implode($this->_getTextualOperator() , $this->_elements) . ')';
      }
      else {
        reset($this->_elements);
        return (string) current($this->_elements);
      }
    }
    return '';
  }
}

/**
 * Represent a simple user term (or phrase).
 */
class Solr_Query_Term extends Solr_Query_FuzzyStatementAbstract
{
  /**
   * Term
   */
  protected $_term = NULL;

  /**
   * Construct new term
   * 
   * @param string $term
   *   Term or value to lookup
   * @param float $boost = NULL
   *   (optional) Float superior to 0
   * @param string $exclusive = NULL
   *   (optional) Solr_Query_Operator::OPERATOR_REQUIRE
   *   or SolrHelper::OPERATOR_PROHIBIT
   * 
   * @throws Solr_Query_Exception
   *   In case of errors
   */
  public function __construct($term, $boost = NULL, $exclusion = NULL) {
    $this->_term = trim((string) $term);
    $this->setExclusion($exclusion);
    $this->setBoost($boost);
  }

  /**
   * (non-PHPdoc)
   * @see Solr_Query_StatementAbstract#_rawOutput()
   */
  protected function _rawOutput() {
    // Be carefull with this, boost won't work when doing a dismax query.
    return Solr_Query_StatementAbstract::escapeToken($this->_term);
  }
}

/**
 * Represent a user term collection.
 */
class Solr_Query_TermCollection extends Solr_Query_Collection
{
  /**
   * Add a term.
   * 
   * @see Solr_Query_Collection#add()
   * 
   * @param string|Solr_Query_Term $term
   *   Term to add
   */
  public function add($term) {
    if (! $term instanceof Solr_Query_Term) {
      parent::add(new Solr_Query_Term($term));
    }
    else {
      parent::add($term);
    }
  }
}

class Solr_Query_RangeStatementAbstract extends Solr_Query_StatementAbstract
{
  /**
   * Range start
   * 
   * @var mixed
   */
  protected $_startElement = NULL;

  /**
   * Range stop
   * 
   * @var mixed
   */
  protected $_stopElement = NULL;

  /**
   * Does range is inclusive?
   * 
   * @var boolean
   *   Default is TRUE
   */
  protected $_inclusive = TRUE;

  /**
   * Set inclusive or exclusive mode.
   * 
   * @param boolean $inclusive
   *   Set to TRUE for inclusive mode, FALSE for exclusive mode.
   */
  public function setInclusive($inclusive) {
    $this->_inclusive = (bool) $inclusive;
  }

  /**
   * Construct a range statement.
   * 
   * If you build the object with both $start and $stop set to NULL, this
   * statement won't been built at all in the final query.
   * 
   * We can, but we won't send [* TO *] useless range.
   *
   * @param NULL|mixed $start
   *   Start value
   * @param NULL|mixed $stop
   *   Stop value 
   */
  public function __construct($start, $stop) {
    $this->_start = $start;
    $this->_stop = $stop;
  }

  /**
   * Render range element. Children classes can override this method in order
   * to format their values.
   */
  protected function _renderElement($value) {
    return $value;
  }

  /**
   * Render range element. Overriding classes must implement this function in
   * order to escape values.
   * 
   * Replace the element by '*' wildcard if empty.
   */
  protected function _escapeElement($value) {
    if (empty($value)) {
      $element = '*';
    }
    else {
      $element = $this->_renderElement($value);
    }
    return Solr_Query_StatementAbstract::escapeToken($element);
  }

  /**
   * (non-PHPdoc)
   * @see Solr_Query_StatementAbstract#_rawOutput()
   */
  protected function _rawOutput() {
    if (!empty($this->_start) || !empty($this->_stop)) {
      if ($this->_inclusive) {
        return '[' . $this->_escapeElement($this->_start) . ' TO ' . $this->_escapeElement($this->_stop) . ']';
      }
      else {
        return '{' . $this->_escapeElement($this->_start) . ' TO ' . $this->_escapeElement($this->_stop) . '}';
      }
    }
    return "";
  }
}

class Solr_Query_DateRange extends Solr_Query_RangeStatementAbstract
{
  /**
   * Construct date range.
   * 
   * If you build the object with both $start and $stop set to NULL, this
   * statement won't been built at all in the final query.
   * 
   * We can, but we won't send [* TO *] useless date range.
   *
   * TODO: added format checks
   * 
   * @param NULL|string $start
   *   If string, awaiting for a legal SolR ISO date format.
   *   If NULL, will be replaced by '*' in the date range.
   * @param NULL|string $stop
   *   If string, awaiting for a legal SolR ISO date format.
   *   If NULL, will be replaced by '*' in the date range. 
   */
  public function __construct($start, $stop) {
    parent::__construct($start, $stop);
  }

  /**
   * (non-PHPdoc)
   * @see Solr_Query_RangeStatementAbstract#_renderElement()
   */
  protected function _renderElement($value) {
    // Nothing to do here.
    return $value;
  }
}

/**
 * Represent a field filter
 */
class Solr_Query_Field extends Solr_Query_StatementAbstract
{
  /**
   * Field name
   * 
   * @var string
   */
  protected $_field = NULL;

  /**
   * Proxy variable to collection because PHP can't do multiple heritage.
   * 
   * @var Solr_Query_TermCollection
   */
  protected $_collection = NULL;

  /**
   * Build new instance
   * 
   * @param string $field
   *   Field on which to apply this filter
   * @param string|array|Solr_Query_StatementAbstract|Solr_Query_Collection $terms
   *   Term(s) to apply for this field
   */
  public function __construct($field, $terms) {
    if ($terms instanceof Solr_Query_Collection) {
      $this->_collection = $terms;
    }
    else if ($terms instanceof Solr_Query_StatementAbstract) {
      $this->_collection = new Solr_Query_Collection();
      $this->_collection->add($terms);
    }
    else if (is_array($terms)) {
      $this->_collection = new Solr_Query_Collection();
      foreach ($terms as $term) {
        if (! $term instanceof Solr_Query_StatementAbstract) {
          $this->_collection->add(new Solr_Query_Term($term));
        }
        else {
          $this->_collection->add($term);
        }
      }
    }
    else {
      $this->_collection = new Solr_Query_Collection();
      $this->_collection->add(new Solr_Query_Term($terms));
    }
    $this->_field = (string) $field;
  }

  /**
   * (non-PHPdoc)
   * @see Solr_Query_StatementAbstract#_rawOutput()
   */
  protected function _rawOutput() {
    if (! empty($this->_collection)) {
      $collection = (string) $this->_collection;
      if (!empty($collection)) {
        return Solr_Query_StatementAbstract::escapeToken($this->_field) . ':' . $collection;
      } 
    }
    return '';
  }
}

/**
 * Query string implementation.
 */
class Solr_QueryString extends Solr_Query_Collection { }
