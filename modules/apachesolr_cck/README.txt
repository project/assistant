
Apache Solr CCK helper
======================

Note
====

Before using it, note that there is an active discussion about this topic at:
http://drupal.org/node/664896

Module Description
==================

This is a simple CCK automatic introspection that react when 'apachesolr' module
hooks are invoked.

This helps users integrating CCK with 'apachesolr' without having to write any
custom code.

Note that we introspect some well known CCK fields, and provide generic ways of
indexing them using the found type. For some fancy or custom CCK fields it just
won't work. This module will ignore types it does not know.

Supported fields types are:
 - number_integer
 - number_decimal
 - number_float
 - text
 - filefield
