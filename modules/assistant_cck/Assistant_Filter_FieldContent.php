<?php

/**
 * @file
 * Solace CCK field content filter implementation.
 */

class Assistant_Filter_FieldContent extends Assistant_FilterAbstract
{
  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#getTitle()
   */
  public function getTitle() {
    return "Field content";
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#getDescription()
   */
  public function getDescription() {
    return "Allow to filter with field content";
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#isBoostAble()
   */
  public function isBoostAble() {
    return TRUE;
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#_build($context, $values, $query)
   */
  protected function _build(Assistant_ContextAbstract $context, &$values, SolrQuery $query) {
    // TODO: phrase handling
    if ($values['field']) {
      if ($values['field_value_phrase']) {
        $field = new Solr_Query_Field(_assistant_cck_get_index_key($values['field']), new Solr_Query_Term($values['field_value']));
        $field->setExclusion(Solr_Query_Operator::OPERATOR_REQUIRE);
        $query->fq->add($field);
      }
      else {
        $field = new Solr_Query_Field(_assistant_cck_get_index_key($values['field']), new Solr_Query_Term($values['field_value'], $values['boost']));
        $query->q->add($field);
      }
    }
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#_form($context, $values)
   */
  protected function _form(Assistant_ContextAbstract $context, &$values = array()) {
    $form = array('#prefix' => '<div class="container-inline">', '#suffix' => '</div>');
    $options = array();
    foreach (apachesolr_cck_get_supported_fields() as $field_name => $row) {
      $options[$field_name] = t($row->label);
    }
    $form['field'] = array(
      '#type' => 'select',
      '#default_value' => $values['field'],
      '#options' => $options,
      '#required' => TRUE,
    );
    $this->setAhahProperty($form['field'], 'change');

    if (isset($values['field'])) {
      $field = content_fields($values['field']);
      $allowed_values = content_allowed_values($field);
      if (!empty($allowed_values)) {
        $form['field_value'] = array(
          '#type' => 'select',
          '#options' => $allowed_values,
          '#default_value' => (isset($values['field_value']) ? $values['field_value'] : NULL),
          // '#multiple' => $field['multiple'], // TODO handle this in build function
          '#required' => TRUE,
        );
        $form['field_value_phrase'] = array(
          '#type' => 'hidden',
          '#default_value' => TRUE,
        );
      }
      else {
        $form['field_value'] = array(
          '#type' => 'textfield',
          '#size' => 40,
          '#default_value' => (isset($values['field_value']) ? $values['field_value'] : NULL),
          '#required' => TRUE,
        );
        $form['field_value_phrase'] = array(
          '#type' => 'checkbox',
          '#title' => t('Exact match'),
          '#default_value' => $values['field_value_phrase'],
          '#description' => t("This will force the query to exclude any content that does not matches the exact entered text. Boost will be ignored."),
        );
      }
    }

    return $form;
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#isFilterQuery()
   */
  public function isFilterQuery() {
    return TRUE;
  }
}
