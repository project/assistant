<?php

/**
 * @file
 * Solace CCK multiple field content filter implementation.
 */

class Assistant_Filter_MultipleFieldContent extends Assistant_FilterAbstract
{
  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#getTitle()
   */
  public function getTitle() {
    return "Field content within multiple fields";
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#getDescription()
   */
  public function getDescription() {
    return "Allow to filter with field content, using multiple fields";
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#isBoostAble()
   */
  public function isBoostAble() {
    return TRUE;
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#_build($context, $values, $query)
   */
  protected function _build(Assistant_ContextAbstract $context, &$values, SolrQuery $query) {
    // TODO: phrase handling
    foreach ($values['field'] as $field_name) {
      if ($field_name) {
        $field = new Solr_Query_Field(
          _assistant_cck_get_index_key($field_name),
          new Solr_Query_Term($values['field_value'], $values['boost'])
        );
        $query->q->add($field);
      }
    }
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#_form($context, $values)
   */
  protected function _form(Assistant_ContextAbstract $context, &$values = array()) {
    $form = array();
    $options = array();
    foreach (apachesolr_cck_get_supported_fields() as $field_name => $row) {
      $options[$field_name] = t($row->label);
    }
    $form['field'] = array(
      '#type' => 'checkboxes',
      '#default_value' => isset($values['field']) ? $values['field'] : array(),
      '#options' => $options,
      '#required' => TRUE,
    );
    $form['field_value'] = array(
      '#type' => 'textfield',
      '#size' => 40,
      '#default_value' => $values['field_value'],
      '#required' => TRUE,
    );
    $form['field_value_phrase'] = array(
      '#type' => 'checkbox',
      '#title' => t('Exact phrase'),
      '#default_value' => $values['field_value_phrase'],
    );
    return $form;
  }
}
