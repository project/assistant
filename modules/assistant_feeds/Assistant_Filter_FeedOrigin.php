<?php

/**
 * @file
 * Solace Feeds module filters implementation.
 */

class Assistant_Filter_FeedOrigin extends Assistant_Filter_MultipleAbstract
{
  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#getTitle()
   */
  public function getTitle() {
    return "Comes from a feed";
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#getDescription()
   */
  public function getDescription() {
    return "Allows to filter search results by origin feed";
  }

  /**
   * This method must return an Apache SolR schema field name.
   * 
   * @return string
   */
  protected function _getQueryField() {
    return _assistant_feeds_index_key();
  }

  /**
   * This method must return a ready to be filled collection statement.
   * 
   * @return Solr_Query_Collection
   */
  protected function _getQueryCollectionStatement() {
    return new Solr_Query_Collection();
  }

  /**
   * This method must return a statement to add in the collection using the
   * given value array.
   * 
   * @param Assistant_ContextAbstract $context
   *   Assistant_ContextAbstract instance.
   * @param array &$values
   *   Values from the form filter element.
   * 
   * @return Solr_Query_StatementAbstract
   */
  protected function _getQueryStatementValue(Assistant_ContextAbstract $context, &$values) {
    return new Solr_Query_Term($values['source']);
  }
  

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#_build($context, $values, $query)
   */
  /*
  protected function _build(Assistant_ContextAbstract $context, &$values, SolrQuery $query) {
    if (! empty($values['source'])) {
      $query->fq->add($field = new Solr_Query_Field(_assistant_feeds_index_key(), new Solr_Query_Term($values['source'])));
    }
  }
  */

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#_form($context, $values)
   */
  protected function _subForm(Assistant_ContextAbstract $context, &$values = array()) {
    $form = array();
    $form['source'] = array(
      '#type' => 'select',
      '#default_value' => isset($values['source']) ? $values['source'] : NULL,
      '#options' => _assistant_feeds_list(),
      '#required' => TRUE,
    );
    return $form;
  }
}
