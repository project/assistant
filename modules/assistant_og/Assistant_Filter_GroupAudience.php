<?php

/**
 * @file
 * Solace OG module filters implementation.
 */

class Assistant_Filter_GroupAudience extends Assistant_FilterAbstract
{
  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#getTitle()
   */
  public function getTitle() {
    return "Belongs to group";
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#getDescription()
   */
  public function getDescription() {
    return "Allows to filter search by group audience";
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#isBoostAble()
   */
  public function isBoostAble() {
    return FALSE;
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#_build($context, $values, $query)
   */
  protected function _build(Assistant_ContextAbstract $context, &$values, SolrQuery $query) {
    if (!empty($values['audience'])) {
      $query->fq->add(new Solr_Query_Field(apachesolr_og_gid_key(), new Solr_Query_Term($values['audience'])));
    }
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#_form($context, $values)
   */
  protected function _form(Assistant_ContextAbstract $context, &$values = array()) {
    $form = array();

    if (isset($values['audience']) && $node = node_load($values['audience'])) {
      $default = $node->nid . ' - ' . filter_xss($node->title);
    }
    // Got this field content without validation process
    else if (preg_match('/^[0-9]+\s+-\s+.*$/', $values['audience'])) {
      $default = $values['audience'];
    }
    else {
      $default = NULL;
    }

    $form['audience'] = array(
      '#type' => 'textfield',
      '#default_value' => $default,
      '#autocomplete_path' => 'assistant/og/autocomplete',
      '#required' => TRUE,
    );

    return $form;
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#validate($context, $values)
   */
  protected function _validate(Assistant_ContextAbstract $context, &$values) {
    $parts = preg_split('/[\s-]+/', trim($values['audience']), 2);
    if (isset($parts[0]) && is_numeric($parts[0])) {
      $values['audience'] = $parts[0];
    }
    else {
      return array('audience', t("Invalid value"));
    }
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#isFilterQuery()
   */
  public function isFilterQuery() {
    return TRUE;
  }
}
