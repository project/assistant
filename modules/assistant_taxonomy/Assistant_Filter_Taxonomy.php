<?php

/**
 * @file
 * Solace OG module filters implementation.
 */

class Assistant_Filter_Taxonomy extends Assistant_Filter_MultipleAbstract
{
  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#getTitle()
   */
  public function getTitle() {
    return "Filters by terms";
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#getDescription()
   */
  public function getDescription() {
    return "Allows to filter search using taxonomy terms";
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_FilterAbstract#isEnabled($context)
   */
  public function isEnabled(Assistant_ContextAbstract $context) {
    // Check for existing vocabularies, and much more, for existing terms in
    // those.
    return (bool) db_result(db_query("SELECT 1 FROM {term_data} td JOIN {vocabulary} v ON v.vid = td.tid LIMIT 1"));
  }

  /**
   * This method must return an Apache SolR schema field name.
   * 
   * @return string
   */
  protected function _getQueryField() {
    return 'tid';
  }

  /**
   * This method must return a ready to be filled collection statement.
   * 
   * @return Solr_Query_Collection
   */
  protected function _getQueryCollectionStatement() {
    return new Solr_Query_TermCollection();
  }

  /**
   * This method must return a statement to add in the collection using the
   * given value array.
   * 
   * @param Assistant_ContextAbstract $context
   *   Assistant_ContextAbstract instance.
   * @param array &$values
   *   Values from the form filter element.
   * 
   * @return Solr_Query_StatementAbstract
   */
  protected function _getQueryStatementValue(Assistant_ContextAbstract $context, &$values) {
    return new Solr_Query_Term($values['tid']);
  }

  /**
   * Build the multiple subform. This method signature is the exact same that
   * the Assistant_FilterAbstract#_form($context, $values) one.
   * 
   * @see Assistant_FilterAbstract#_form($context, $values)
   */
  protected function _subForm(Assistant_ContextAbstract $context, &$values = array()) {
    $form = array();

    $options = array(NULL => t('-- Select vocabulary --'));
    foreach (taxonomy_get_vocabularies() as $vid => $voc) {
      // Include vocabulary only if it has terms
      if (db_result(db_query("SELECT COUNT(*) FROM {term_data} WHERE vid = %d", array($vid)))) {
        $options[$vid] = check_plain($voc->name);
      }
    }

    $form['vid'] = array(
      '#type' => 'select',
      '#options' => $options,
      // '#required' => TRUE,
      '#default_value' => (isset($values['vid']) ? $values['vid'] : NULL),
      '#weight' => 0,
    );
    $this->setAhahProperty($form['vid'], 'change');

    if (isset($values['vid']) && !empty($values['vid'])) {
      $form['tid'] = _taxonomy_term_select(NULL, 'name', NULL, $values['vid'], NULL, FALSE, NULL);
      $form['tid']['#required'] = TRUE;
      $form['tid']['#weight'] = 10;
      if (isset($values['tid'])) {
        $form['tid']['#default_value'] = $values['tid'];
      }
      if (!$form['tid']['#multiple']) {
        array_unshift($form['tid']['#options'], t('-- Select term --'));
      }
    }

    return $form;
  }
}
