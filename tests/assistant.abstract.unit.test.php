<?php

/**
 * @file
 * Common fonctions for Solace unit test
 */

class AbstractAssistantUnitTestCase extends DrupalUnitTestCase
{
  private $__modulesDir = array();

  protected function getModuleDir($module) {
    // Let errors happens if wrong usage, because we are writing unit tests and
    // so if they are wrong, SimpleTest should alert us that we are writing some
    // crappy test code.
    return $this->__modulesDir[$module];
  }
  
  protected function requireModule($module) {
    // Let errors happens if wrong usage, because we are writing unit tests and
    // so if they are wrong, SimpleTest should alert us that we are writing some
    // crappy test code.
    require_once $this->__modulesDir[$module] . '/' . $module . '.module';
  }

  protected function requireModuleFile($module, $file) {
    // Let errors happens if wrong usage, because we are writing unit tests and
    // so if they are wrong, SimpleTest should alter us that we are writing some
    // crappy test code.
    require_once $this->__modulesDir[$module] . '/' . $file;
  }

  private $__sampleDir = NULL;

  protected function getSamplesDir($module) {
    if (isset($this->__modulesDir[$module])) {
      return $this->__modulesDir[$module];
    }
    return FALSE;
  }

  public function setUp() {
    parent::setUp();

    // Populate usefull dirs
    foreach (array('assistant_api') as $module) {
      $this->__modulesDir[$module] = drupal_get_path('module', $module);
    }
    // $this->__sampleDir = $this->__modulesDir['assist'] . '/samples';

    // Load some needed includes
    $this->requireModule('assistant_api');
  }

  /**
   * Load a file content buffer
   * 
   * @param string $file
   *   File name
   * @param string $dir = NULL
   *   Directory full path. If NULL given uses the sample path.
   * 
   * @return string
   *   File content
   */
  protected function _loadFileContent($file, $dir = NULL) {
    if ($dir !== NULL) {
      $path = $dir . '/' . $file;
    }
    else {
      $path = $this->__sampleDir . '/' . $file;
    }
    // Let errors happens if wrong usage, because we are writing unit tests and
    // so if they are wrong, SimpleTest should alert us that we are writing some
    // crappy test code.
    return file_get_contents($path);
  }
}
