<?php

/**
 * @file
 * Common fonctions for Solace functional web test
 */

class AbstractAssistantWebTestCase extends DrupalWebTestCase
{
  private $__modulesDir = array();

  protected function getModuleDir($module) {
    // Let errors happens if wrong usage, because we are writing unit tests and
    // so if they are wrong, SimpleTest should alert us that we are writing some
    // crappy test code.
    return $this->__modulesDir[$module];
  }

  protected function requireModuleFile($module, $file) {
    // Let errors happens if wrong usage, because we are writing unit tests and
    // so if they are wrong, SimpleTest should alter us that we are writing some
    // crappy test code.
    require_once $this->__modulesDir[$module] . '/' . $file;
  }

  public function setUp() {
    parent::setUp('assistant_api');

    // Populate usefull dirs
    foreach (array('assistant_api') as $module) {
      $this->__modulesDir[$module] = drupal_get_path('module', $module);
    }
  }
}
