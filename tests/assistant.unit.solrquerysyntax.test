<?php

/**
 * @file
 * Solace Solr Query subsystem unit test
 */

require_once __DIR__ . '/assistant.abstract.unit.test.php';

class SolrQuerySyntaxTestCase extends AbstractAssistantUnitTestCase
{
  public static function getInfo() {
    return array(
      'name' => t("SolrQuerySyntax"),
      'description' => t("Will test SolR Query Syntax builder OOP abstraction"),
      'group' => t("Assistant"),
    );
  }

  public function setUp() {
    parent::setUp();
    $this->requireModuleFile('assistant_api', 'SolrQuerySyntax.inc');
  }

  public function testSimpleQueryBuild() {
    $reference = "(foo bar)";
    $f_query = new Solr_QueryString();
    $f_termCollection = new Solr_Query_TermCollection();
    $f_query->add($f_termCollection);
    $term = new Solr_Query_Term("foo");
    $f_termCollection->add($term);
    $term = new Solr_Query_Term("bar");
    $f_termCollection->add($term);
    $s_query = new Solr_QueryString();
    $s_termCollection = new Solr_Query_TermCollection();
    $s_termCollection->add('foo');
    $s_termCollection->add('bar');
    $s_query->add($s_termCollection);
    $this->assertEqual((string)$s_query, (string)$f_query, 'Queries match');
    $this->assertEqual($reference, (string)$f_query, 'Queries match reference');

    $reference = "(foo OR bar)";
    $f_termCollection->setOperator(Solr_Query_Operator::OPERATOR_OR);
    $s_termCollection->setOperator(Solr_Query_Operator::OPERATOR_OR);
    $this->assertEqual((string)$s_query, (string)$f_query, 'Queries match');
    $this->assertEqual($reference, (string)$f_query, 'Queries match reference');

    $reference = "(foo AND bar)";
    $f_termCollection->setOperator(Solr_Query_Operator::OPERATOR_AND);
    $s_termCollection->setOperator(Solr_Query_Operator::OPERATOR_AND);
    $this->assertEqual((string)$s_query, (string)$f_query, 'Queries match');
    $this->assertEqual($reference, (string)$f_query, 'Queries match reference');

    $reference = "(foo bar)";
    $f_termCollection->setOperator(NULL);
    $s_termCollection->setOperator(NULL);
    $this->assertEqual((string)$s_query, (string)$f_query, 'Queries match');
    $this->assertEqual($reference, (string)$f_query, 'Queries match reference');
  }

  public function testSubQueryBuild() {
    $reference = "((foo bar) AND baz:rex)";
    $query = new Solr_QueryString();
    $query->setOperator(Solr_Query_Operator::OPERATOR_AND);
    $first = new Solr_Query_TermCollection();
    $first->add(new Solr_Query_Term('foo'));
    $first->add('bar');
    $second = new Solr_Query_Field('baz', array('rex'));
    $query->add($first);
    $query->add($second);
    $this->assertEqual((string)$query, $reference, 'Query matches reference');

    $reference = "(foo OR (baz AND (rex toto)))";
    $query = new Solr_QueryString();
    $query->setOperator(Solr_Query_Operator::OPERATOR_OR);
    $first = new Solr_Query_Term('foo');
    $query->add($first);
    $second = new Solr_QueryString();
    $second->setOperator(Solr_Query_Operator::OPERATOR_AND);
    $second->add(new Solr_Query_Term('baz'));
    $termCollection = new Solr_Query_TermCollection();
    $termCollection->add('rex');
    $termCollection->add(new Solr_Query_Term('toto'));
    $second->add($termCollection);
    $query->add($second);
    $this->assertEqual((string)$query, $reference, 'Query matches reference');
  }

  public function testBoostAndFuzzy() {
    try {
      $reference = '"test"~1';
      $query = new Solr_QueryString();
      $term = new Solr_Query_Term("test");
      $term->setFuzzyness(1);
      $query->add($term);
      $this->assertEqual($reference, (string)$term, 'Term matches reference');
      $this->assertEqual($reference, (string)$query, 'Query string matches reference');
    }
    catch (Solr_Query_Exception $e) {
      $this->fail('Catched unattended exception: ' . $e->getMessage());
    }

    try {
      $reference = '"foo"^10000';
      $query = new Solr_QueryString();
      $term = new Solr_Query_Term("foo");
      $term->setBoost(10000);
      $query->add($term);
      $this->assertEqual($reference, (string)$term, 'Term matches reference');
      $this->assertEqual($reference, (string)$query, 'Query string matches reference');
      $this->assert(TRUE, (string)$term);
    }
    catch (Solr_Query_Exception $e) {
      $this->fail('Catched unattended exception: ' . $e->getMessage());
    }
    
    try {
      $reference = '"test"~10^0.1';
      $query = new Solr_QueryString();
      $term = new Solr_Query_Term("test");
      $term->setFuzzyness(10);
      $term->setBoost(0.1);
      $query->add($term);
      $this->assertEqual($reference, (string)$term, 'Term matches reference');
      $this->assertEqual($reference, (string)$query, 'Query string matches reference');
      $this->assert(TRUE, (string)$term);
    }
    catch (Solr_Query_Exception $e) {
      $this->fail('Catched unattended exception: ' . $e->getMessage());
    }
    
    try {
      $reference = '("apache solr"~4^10 "lucene"^10000)';
      $termCollection = new Solr_Query_TermCollection();
      $term = new Solr_Query_Term("apache solr");
      $term->setRoaming(4);
      $term->setBoost(10);
      $termCollection->add($term);
      $termCollection->add(new Solr_Query_Term("lucene", 10000));
      $this->assertEqual($reference, (string)$termCollection, 'Term collection matches reference');
    }
    catch (Solr_Query_Exception $e) {
      $this->fail('Catched unattended exception: ' . $e->getMessage());
    }
  }

  public function testRequireProhibit() {
  }

  public function testRange() {
  }
}

